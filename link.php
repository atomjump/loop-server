<?php

//Email confirmation
require('config/db_connect.php');

require("classes/cls.basic_geosearch.php");
require("classes/cls.layer.php");
require("classes/cls.ssshout.php");

//Confirm the user and password are correct


$lg = new cls_login();

$main_message = $lg->email_confirm($_REQUEST['d']);

if(isset($_REQUEST['id']) && isset($_REQUEST['devicetype'])) {
	//We are also trying to pair with the app at the same time. This request
	//would only come if you have the 'notifications' plugin installed.
	global $root_server_url;
	$url = trim_trailing_slash($root_server_url) . "/plugins/notifications/register.php?id=" . urlencode($_REQUEST['id']) . "&devicetype=" . urlencode($_REQUEST['devicetype']);
	
	if(isset($_REQUEST['d'])) {
	
		 $url .= "&d=" . urlencode($_REQUEST['d']);
	}
	
	//Redirect to the registration of the app.
	header("Location: " . $url, true, 301);
	exit(0);
}


$first_button_wording = "&#8962;";		//A 'home' UTF-8 char



$follow_on_link = "https://atomjump.com";
if($cnf['serviceHome']) {
	$follow_on_link = add_subdomain_to_path($cnf['serviceHome']);
}

$follow_on_link = $follow_on_link . "?autostart=true";		//Open the forum automatically if atomjump.com


if(isset($msg['msgs'][$lang]['backHome'])) {		//"Go to the forum"
	$first_button_wording = $msg['msgs'][$lang]['backHome'];
} 

$first_button = $follow_on_link;




 include("components/basic-page.php");
?>
