<?php

	//Run this as a CRON job once a day
	//sudo crontab -e
	//Add the line e.g.
	//0 0 * * * 	/usr/bin/php /var/www/html/your-atomjump-api/update-vid-server-cnt.php
	
	require('config/db_connect.php');
	
	global $cnf; 

	echo "Getting latest number of video servers.  Use 'php update-vid-server-cnt.php --force' to skip the random 100 minute pause.\n";
	if((isset($argv[1])) && ($argv[1] == "--force")) {
		//Don't pause
	} else {
		$pause = rand(1,6000);		//Increase for more seconds randomness. 600/60 = 10 minutes
		sleep($pause);
	}

	if(isset($cnf['video']['totalVideoServersUrl'])) {
		$url = $cnf['video']['totalVideoServersUrl'];
		
		//Read the remote url
		$new_server_cnt = (int) file_get_contents($url);
		echo "New server count: " . $new_server_cnt . "\n";
		$old_server_cnt = (int) $cnf['video']['totalVideoServers'];
		
		if(($new_server_cnt !== $old_server_cnt)&&(is_integer($new_server_cnt))) {
			//Rewrite the config file
			//Read the current
			$full_config_str = file_get_contents(__DIR__ . "/config/config.json");
			$full_config = json_decode($full_config_str, true);
			
			//Set the new value
			$full_config['staging']['video']['totalVideoServers'] = $new_server_cnt;
			$full_config['production']['video']['totalVideoServers'] = $new_server_cnt;
			
			$json = json_encode($full_config, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
			file_put_contents(__DIR__ . "/config/config.json", $json);
			
			echo "Updated to " . $new_server_cnt . "\n";
		} else {
			echo "Still " . $old_server_cnt . " servers.\n";
		}
	} 
	
	
?>
