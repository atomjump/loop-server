<?php
	//This is called by the .htaccess in /images/im/.htaccess 
	/*
			#Handle missing images in a cluster. CHANGE THIS BELOW
			#Insert your own relative path by replacing /api/ below if your loop-server install is at a different subfolder than /api/
			ErrorDocument 404 /api/missing-image.php
	*/
	//Note: this can now handle .pdf, .mp4 and .mp3 media files, also.

	include_once(__DIR__ . '/config/db_connect.php');		
	
	global $local_server_path;
	global $root_server_url;
	global $cnf;
	
	$verbose = false;		//Usually false in live environs
	
	function is_image($pathToFile)
	{
	  if( false === exif_imagetype($pathToFile) ) return false;

	   return true;
	}
	
	function get_remote($url, $filename) {
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);		        
		if(curl_error($curl))
		{
			error_log('error:' . curl_error($curl));
		}
	
		curl_close ($curl);
		
		return $response;
	}
	
	
	
	//Special case check for .html file, rather than an image file. Particularly useful for the server capacity checking
	if(substr($_SERVER['REQUEST_URI'], -5) == ".html") {
		//Return a 404 error, file not found
		http_response_code(404);
		die();
	}
						
	if(isset($cnf['uploads']['imagesShare'])) {
			//Share across our own servers
			
			$specific_server = '';
			
			//Get the domain of the web url, and replace with ip:80
			$parse = parse_url($root_server_url);
			$domain = $parse['host'];
					
			if($specific_server == '') {  //Defaults to all
				$servers = array();
				for($cnt =0; $cnt< count($cnf['ips']); $cnt++) {
					$server_url = str_replace($domain, $cnf['ips'][$cnt] . ":" . $cnf['uploads']['imagesShare']['port'], $root_server_url) . "/";
					if($cnf['uploads']['imagesShare']['https'] == false) {
						//Only do with http
						$server_url = str_replace("https", "http", $server_url);
					}
					$servers[] = $server_url;
					
				}
				
			} else {
				//Only process this one server
				$servers = array($specific_server);
		
			}
			
			$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
			if($verbose == true) error_log("input path:" . $path);
			
			//E.g. Input $path = /api/images/property/test.jpg
			
		
			
			
			$webroot = web_root($cnf);		//Include potential subdomains
			$image_path = trim_trailing_slash($webroot) . "/images";
			$image_path = parse_url($image_path, PHP_URL_PATH);
			//E.g. $image_path = /api/images
			
			if($verbose == true) error_log("image path:" . $image_path);
			$path = str_replace($image_path, "", $path);
			//E.g. Target for path from  is /im/test.jpg
			if($verbose == true) error_log("path:" . $path);

			if(strpos($path, "_HI") !== false) {
				//We are trying to show the HI-res version of the image. This may not be there, in which case we want to show the lo-res version
				$non_hi_path = str_replace("_HI", "", $path);
			} else {
				$non_hi_path = $path;
			}

			// extracted basename
			$filename = basename($path);
			
			//$filename is now e.g. "upl-123443.jpg"
			
		
			
			$random_count = count($servers);
			
			$failure_getting = true;
			
			//Maximum 3 attempts from different cluster servers. On the 4th attempt, try the non-hi version
			for($cnt = 0; $cnt < 4; $cnt++) {
				
				if($cnt == 3) {
					//4th attempt - it could be we have removed the _HI version, so try without it.
					$path = $non_hi_path;
				}
				
				$server_num = rand(1, $random_count) % $random_count;
            if($verbose == true) error_log("checking server num:" . $server_num);

				$server = $servers[$server_num];
				if($config['usingStaging'] == true) {
					//Staging special case
					$server = add_trailing_slash($webroot);
				}
				if($verbose == true) error_log("checking server:" . $server);
				
				
				if($server) {
					$url  = trim_trailing_slash($server) . "/images" . $path;		
					//E.g. $url = "https://staging.atomjump.com/api/images/im/upl440-47456560.jpg";
					
					if($verbose == true) error_log("url:" . $url);
					
					$img = __DIR__ . '/images' . $path;
					
					//Download and put into our local image folder
					
					
					try {
						//Do a file check request first
						$checker = trim_trailing_slash($server) . "/image-exists.php?image=" . $path . "&code=" . $cnf['uploads']['imagesShare']['checkCode'];	
						if($verbose == true) error_log("checker: " . $checker );
						
						$checker_str = get_remote($checker, $path);
						if($verbose == true) error_log("checked response: " . $checker_str );
						// Check if file exists
						if($checker_str !== "true"){
							if($verbose == true) error_log("File not found");
							$failure_getting = true;
						} else{
							if($verbose == true) error_log("File exists");
							$str_image = get_remote($url, $path);
							if($str_image === false) {
								if($verbose == true) error_log("Failed to get"); 
								$failure_getting = true;
							} else {
								//Put contents into local file
								if(file_put_contents($img, $str_image)) {
									if(is_image($img)) {
										//Pipe the image back to the browser
										if($verbose == true) error_log("Is an image, piping out");
										header('Content-type: image/jpeg');
										readfile($img);
										exit(0);
									} else {
										//Check if we are one of the other options, .pdf, but not .mp3, .mp4 as they cannot be easily cached if they are streaming.
										if((strpos($img, ".mp4") !== false)||(strpos($img, ".mp3") !== false)) {
											//A video or audio clip. Don't directly respond, but allow the reader to attempt again in a few seconds
											//Do nothing, at least the file should be available now.
										} else {
											if(strpos($img, ".pdf") !== false) {
												//A pdf doc
												header('Content-type: application/pdf');
												readfile($img);
												exit(0);
											} else {
													//None of the above?
													//Remove the file
													if($verbose == true) error_log("Is not a media file, removing the file downloaded");
													unlink($img);
													$failure_getting = true;
												
											}
										}
										
									}
								} else {
									//Failed file_put_contents
									$failure_getting = true;
								}
							}
						}
   						
   						
						
					} catch (Exception $e) {
						if($verbose == true) error_log("Failed to get " . $e->getMessage()); 
						$failure_getting = true;
					}
										
				}
								
			}
			
			//Tried all the attempts
			if($failure_getting == true) {
				//Can put a default blank image in here
				if($verbose == true) error_log("Putting default in");
				
				$img = __DIR__ . '/images/im/default.jpg';
				header('Content-type: image/jpeg');
				readfile($img);
				exit(0);
			}
	} else {	
		//Can first see if there is a non _HI version
		$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		if($verbose == true) error_log("input path:" . $path);		
		//E.g. Input $path = /api/images/property/test.jpg
		
		$webroot = web_root($cnf);		//Include potential subdomains
		$image_path = trim_trailing_slash($webroot) . "/images";
		$image_path = parse_url($image_path, PHP_URL_PATH);
		//E.g. $image_path = /api/images
		
		if($verbose == true) error_log("image path:" . $image_path);
		$path = str_replace($image_path, "", $path);
		//E.g. Target for path from  is /im/test.jpg
		if($verbose == true) error_log("path:" . $path);
		
		$img = __DIR__ . '/images' . $path;	
		
		
		if(strpos($img, "_HI") !== false) {
			//We are trying to show the HI-res version of the image. This may not be there, in which case we want to show the lo-res version
			$non_hi_img = str_replace("_HI", "", $img);
		} else {
			$non_hi_img = $img;
		}		
				
		if(file_exists($non_hi_img)) {
			//Return the non-hi version
			header('Content-type: image/jpeg');
			readfile($non_hi_img);
			exit(0);
		} else {
	
			//Can put a default blank image in here
			$img = __DIR__ . '/images/im/default.jpg';
			
			header('Content-type: image/jpeg');
			readfile($img);
			exit(0);
		}
	
	}


?>
