<?php

  /* 
  		The main configuration and database connection script, that is included by all
  		other files.
  
  */
  if(!isset($config)) {
     //Get global config - but only once
     $data = file_get_contents (dirname(__FILE__) . "/config.json");
     if($data) {
        $config = json_decode($data, true);
        if(!isset($config)) {
          echo "Error: config/config.json is not valid JSON.";
          exit(0);
        }
     
     } else {
       echo "Error: Missing config/config.json.";
       exit(0);
     
     } 
  }
  
  function file_get_contents_utf8($fn) {
     $content = file_get_contents($fn);
      return mb_convert_encoding($content, 'UTF-8',
          mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
  }
	

  if(!isset($msg)) {
     //Get global language file - but only once
     $data = file_get_contents_utf8(dirname(__FILE__) . "/messages.json");
     if($data) {
        $msg = json_decode($data, true);
        if(!isset($msg)) {
          echo "Error: config/messages.json is not valid JSON ";
          
          switch(json_last_error()) {
          
                case JSON_ERROR_NONE:
                    echo ' - No errors';
                break;
                case JSON_ERROR_DEPTH:
                    echo ' - Maximum stack depth exceeded';
                break;
                case JSON_ERROR_STATE_MISMATCH:
                    echo ' - Underflow or the modes mismatch';
                break;
                case JSON_ERROR_CTRL_CHAR:
                    echo ' - Unexpected control character found';
                break;
                case JSON_ERROR_SYNTAX:
                    echo ' - Syntax error, malformed JSON';
                break;
                case JSON_ERROR_UTF8:
                    echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
                default:
                    echo ' - Unknown error';
                break;
          
          }
          exit(0);
        }
        
       
        
     
     } else {
       echo "Error: Missing messages/messages.json.";
       exit(0);
     
     } 
  }
  
	function scale_up_horizontally_check($cnf)
	{
		$this_db_cnf = $cnf['db'];

		if(isset($_REQUEST['passcode'])) {
			$layer_name = $_REQUEST['passcode'];			
		}
		
		if(isset($_REQUEST['uniqueFeedbackId'])) {
			$layer_name = $_REQUEST['uniqueFeedbackId'];
		}

		if((isset($this_db_cnf['scaleUp']))&&(isset($layer_name))) {	
			//We are scaling up
			for($cnt = 0; $cnt< count($this_db_cnf['scaleUp']); $cnt ++) {	
				if(preg_match("/" . $this_db_cnf['scaleUp'][$cnt]['labelRegExp'] . "/",$layer_name, $matches) == true) {
					//Override with this database					
					$new_db_cnf = $this_db_cnf['scaleUp'][$cnt];
					
					//Check if we have our own unique plugins enabled for this installation
					if($new_db_cnf['plugins']) {
						global $cnf;
						$cnf['plugins'] = $new_db_cnf['plugins'];
					}
					return $new_db_cnf;
				}

			}
		}
		return $this_db_cnf;
	} 
  


    

    function trim_trailing_slash($str) {
        return rtrim($str, "/");
    }
    
    function add_trailing_slash($str) {
        //Remove and then add
        return rtrim($str, "/") . '/';
    }
    
    
    function check_subdomain($main_page = false)
	{
	 	global $config;
	
		//Check if we have a subdomain - return false if no, or the name of the subdomain if we have
		$host_name = explode(":", $_SERVER['HTTP_HOST']);	//Note: caution as HTTP_HOST can be spoofed in the http headers
		$server_name = $host_name[0];
		
		if(!$server_name) {
			//Try SERVER_NAME version instead
			$server_name = $_SERVER['SERVER_NAME'];
		}

		if(($server_name == $config['staging']['webDomain']) ||
		   ($server_name == $config['production']['webDomain'])) {
		   return false;
		} else {
		
			 
			$tempstr = str_replace(".atomjump.com","",$server_name);   
			$subdomain = str_replace(".ajmp.co","",$tempstr);		//Or alternatively the shorthand version
			
			if($main_page == true) {
				//This is an addition for our private db_connect, only, and applies when we want to query e.g. http://newsubdomain.subdomain.atomjump.com
				//and the [newsubdomain] becomes the subdomain that we request.
				$exploded_subdomain = explode(".", $subdomain);
				$subsubdomain = null;
				if(count($exploded_subdomain) > 1) {
					$subdomain = $exploded_subdomain[1];
					$subsubdomain = $exploded_subdomain[0];
				}
				
				return array($subdomain, $subsubdomain);
			} else {
				return $subdomain;
			}	
		}
		
	
	}
    
    /*
    function check_subdomain()
	{
	 	global $config;
	
		//Check if we have a subdomain - return false if no, or the name of the subdomain if we have
		$server_name = $_SERVER['SERVER_NAME'];

		if(($server_name == $config['staging']['webDomain']) ||
		   ($server_name == $config['production']['webDomain'])) {
		   return false;
		} else {
		
			 
			$tempstr = str_replace(".atomjump.com","",$server_name);   
			$subdomain = str_replace(".ajmp.co","",$tempstr);		//Or alternatively the shorthand version
			
			return $subdomain;
		}
		
	
	}*/
    
    function add_subdomain_to_path($path) {
			//This may be used for modifying the fast server web address
			$subdomain = check_subdomain();
			
			if($subdomain != false) {
				$add_in = $subdomain . ".";
				$new_path = str_replace('[subdomain]', $add_in, $path);
			} else {
				$new_path = str_replace('[subdomain]', "", $path);
			}
			return $new_path;			
	}	


 	error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED); 
 	
  	
 
 
 
 

	$db_name = $config['production']['db']['name'];			//Unless on the cloud below
	if(!isset($notify)) {
		$notify = true;					//Notify users by email and sms, unless we are specified differently
	}



			
	//Defaults to the master production db		
	$db_username = $config['production']['db']['user']; //Edit this e.g. "peter"
	$db_password = $config['production']['db']['pass']; //Edit this e.g. "secretpassword"
	$db_host =  $config['production']['db']['hosts'][0]; 
	$db_name = $config['production']['db']['name'];
	
	$server_timezone = $config['production']['timezone'];
	$db_timezone = $server_timezone;
	$db_cnf = array();		//Make a global		
  
  

	if(($_SERVER["SERVER_NAME"] == $config['staging']['webDomain'])||($staging == true)||($config['usingStaging'] == true)) {
		//Staging
		$staging = true;
		
		$cnf = $config['staging'];
		$db_cnf = $cnf['db'];
		$db_cnf = scale_up_horizontally_check($cnf);

		
		$db_username = $db_cnf['user']; //Edit this e.g. "peter"
		$db_password = $db_cnf['pass']; //Edit this e.g. "secretpassword"
		$db_host =  $db_cnf['hosts'][0]; 
		$db_name = $db_cnf['name'];
	
	
		$root_server_url = trim_trailing_slash(add_subdomain_to_path($cnf['webRoot']));
		$local_server_path = add_trailing_slash($cnf['fileRoot']);
			
		$db_total = count($db_cnf['hosts']);			//Total number of databases
		$max_db_attempts = 2;	//Maximum incremental attempts 
		if(((isset($db_read_only))&&($db_read_only == true))||  //This variable is set by caller scripts in forced read-only situations
		   (isset($db_cnf['singleWriteDb'])&&($db_cnf['singleWriteDb'] === false))) { 		
				$db_num = mt_rand(0,($db_total-1));		//If you add more DB nodes, increase this number
				$db_inc = true;
			
		} else {
			//Only one write db which is db 0
			$db_num = 0;
			$db_inc = false;
			
		}
		
		
		$db_timezone = $db_cnf['timezone'];
		
	} else {
  
        $cnf = $config['production']; 
		$root_server_url = trim_trailing_slash(add_subdomain_to_path($cnf['webRoot']));
		$local_server_path = add_trailing_slash($cnf['fileRoot']);
		
		$db_cnf = $cnf['db'];
		$db_cnf = scale_up_horizontally_check($cnf);



		$db_username = $db_cnf['user']; //Edit this e.g. "peter"
		$db_password = $db_cnf['pass']; //Edit this e.g. "secretpassword"
		$db_host =  $db_cnf['hosts'][0]; 
		$db_name = $db_cnf['name'];
		
		//Live 
		$db_total = count($db_cnf['hosts']);			//Total number of databases
		$max_db_attempts = 2;	//Maximum incremental attempts 
		if(((isset($db_read_only))&&($db_read_only == true))||  //This variable is set by caller scripts in forced read-only situations
		   (isset($db_cnf['singleWriteDb'])&&($db_cnf['singleWriteDb'] === false))) { 		
				$db_num = mt_rand(0,($db_total-1));		//If you add more DB nodes, increase this number
				$db_inc = true;
			
		} else {
			//Only one write db which is db 0
			$db_num = 0;
			$db_inc = false;
			
		}
		$db_host = $db_cnf['hosts'][$db_num];	
		
			
		$db_timezone = $db_cnf['timezone'];
		
		$staging = false;
	}	
	
	//Make sure we have a default logoutPort
	if(!isset($cnf['logoutPort'])) {
	    $cnf['logoutPort'] = 1444;
	}
	
	//General globals:
	$process_parallel = array();                //An array of system commands to run after an insert message request. These
	                                            //are set by any of the plugins, and are run right at the end of the script.
	
	$process_parallel_url = false;              //Used by plugins to run a process after everything else has finished in parallel. Set to true
	                                            //if this is to be run (currently only works for http servers, not https)
	

	

	if($db_cnf['ssl']) {
		$db_ssl = $db_cnf['ssl'];
	} else {
		$db_ssl = null;
	} 
	
	if($db_cnf['port']) {
		$db_port = $db_cnf['port'];
	} else {
		$db_port = null;
	} 
	
	

	//Leave the code below - this connects to the database
	$db = dbconnect($db_host, $db_username, $db_password, null, $db_ssl, $db_port);	
			
	if(!$db) {
		
		if($db_inc == true) {
			$cnt = 0;
			while((!$db) && ($cnt < $max_db_attempts)) {
				$db_num ++;
				if($db_num >= $db_total) $db_num = 0;
				$cnt++;
				//Loop through all the other databases and check if any of them are available - to a max number of attempts				
				$db_host = $db_cnf['hosts'][$db_num];			
				$db = dbconnect($db_host, $db_username, $db_password, null, $db_ssl, $db_port);
			}
			
			if($cnt >= $max_db_attempts) {
				//Let the haproxy know our mysql and therefore server is down
				http_response_code(503);
				exit(0);
			
			}
		} else {
			//Let the haproxy know our mysql and therefore server is down
			http_response_code(503);
			exit(0);
		}
	}
	dbselect($db_name);
	db_set_charset('utf8mb4');		//was utf8
	db_misc();


	if(!isset($start_path)) {
		$start_path = $local_server_path;
	}


	ini_set('session.gc_maxlifetime', 60*60*24*365*3);// expand
	ini_set('session.cookie_lifetime', 60*60*24*365*3);// expand

	require_once $start_path . 'classes/cls_php_session.php';	
	
	$session_class = new php_Session;
   	session_set_save_handler(array(&$session_class, 'open'),
                         array(&$session_class, 'close'),
                         array(&$session_class, 'read'),
                         array(&$session_class, 'write'),
                         array(&$session_class, 'destroy'),
                         array(&$session_class, 'gc'));
	
	//Note: this value  definitely gets passed in as 'sessionId', not 'ses' (which is the name of the cookie)
	if(isset($_REQUEST['sessionId'])&&($_REQUEST['sessionId'] != "")){
		session_id($_REQUEST['sessionId']);
	}

	if(session_start()) {
		$using_session = true;
	} else {
		$using_session = false;
	}
	
	//Set default language, unless otherwise set
  	$lang = $msg['defaultLanguage'];
	if((isset($_COOKIE['lang']))&&($_COOKIE['lang'] != "")) {
	   $lang = $_COOKIE['lang'];
	}
		
	
	//Set our own default language from a caller script
	if((isset($_REQUEST['lang']))&&($_REQUEST['lang'] != "")) {
		//If cookie version of $lang is not already set, use this passed in lang (though doesn't store in cookie)
		if((!isset($_COOKIE['lang']))||($_COOKIE['lang'] == "")) { 
			$lang = $_REQUEST['lang'];
				
			//Set the cookie for follow-on requests from e.g. loop-server-fast
			//Sanitise
			$name = "lang";
			$value = clean_data($lang);
			$params = "";

			//Old-style (prior to PHP7.3) header set directly:
			$days_forward = "+" . ((365*3)+2) . " day";		//The +2 is so we can compare the cookie set via the server with the client side cookies
			$expires = date("D, d M Y H:i:s",strtotime($days_forward)) . ' GMT';
			$header = "Set-Cookie: " . $name . "=" . $value . "; expires=" . $expires . "; Path=/; SameSite=Strict;";
			header($header);		//E.g. 'Set-Cookie: mycookie=value; SameSite=Strict;'
				
		}
	}
	
	function clean_data($string) {
	  //Use for cleaning input data before addition to database
	  if (get_magic_quotes_gpc()) {
	    $string = stripslashes($string);
	  }
	  $string = strip_tags($string);
	  return db_real_escape_string($string);
	}
	
	function clean_data_keep_tags($string)
	{
	
    	  //Use for cleaning input data before addition to database
	  if (get_magic_quotes_gpc()) {
	    $string = stripslashes($string);
	  }
	  
	  return db_real_escape_string($string);
	
	
	}
	
	function clean_output($string) {
	  //Use for cleaning data before display
	  $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
	  return $string;
	}
	
	
	function sanitize_url($input) {
		//Make sure a URL does not include bad chars
		return filter_input($input, FILTER_SANITIZE_URL);
	}
	
	
	
	function make_writable_db()
    	{
    		global $staging;
    		global $cnf;
    		global $db_host;
	    	global $db_username;
	    	global $db_password;
	    	global $db_name;
	    	global $db;
	    	global $db_cnf;
	    	global $db_total;
    	
    	
    	
    		//Ensure we don't need this functionality on a multi-write server - which is always writable, single node,
    		//or a single database server, or a forced read-only override section of code ($db_read_only)
    		if(($db_total === 1)||
    		   (isset($db_cnf['singleWriteDb'])&&($db_cnf['singleWriteDb'] === false))) {
    			
    			if($db) {
    				//Leave the current database
    				return;
    			
    			} else {
    				//We need to reconnect at this point - it is likely at the end of a session
    				$max_db_attempts = 2;	//Maximum incremental attempts 
					
					if($db_total === 1) {
						$db_num = 0;
						$db_inc = false;
					} else {
						$db_num = mt_rand(0,($db_total-1));		//If you add more DB nodes, increase this number
						$db_inc = true;
					}
					
					
					
					$db_host = $db_cnf['hosts'][$db_num];
					
					while((!$db) && ($cnt < $max_db_attempts)) {
						$db_num ++;
						if($db_num >= $db_total) $db_num = 0;
						$cnt++;
						//Loop through all the other databases and check if any of them are available - to a max number of attempts				
						$db_host = $db_cnf['hosts'][$db_num];	
						
						if($db_cnf['ssl']) {
							$db_ssl = $db_cnf['ssl'];
						} else {
							$db_ssl = null;
						} 

						if($db_cnf['port']) {
							$db_port = $db_cnf['port'];
						} else {
							$db_port = null;
						} 
														
						$db = dbconnect($db_host, $db_username, $db_password, null, $db_ssl, $db_port);
					}
					
					dbselect($db_name);
	  				db_set_charset('utf8mb4');
	  				db_misc();
					return;
				}
			}
			

    
	    	//Double check we are connected to the master database - which is writable. Note this is single write database specific
	    	$db_master_host = $db_cnf['hosts'][0];
	    	if(($db_host != $db_master_host)||(!isset($db))) {
	    		//Reconnect to the master db to carry out the write operation
	    		dbclose();		//close off the current db
	    		
	    		$db_host = $db_master_host;
	    			    		
	    		if($db_cnf['ssl']) {
					$db_ssl = $db_master_host['ssl'];
				} else {
					$db_ssl = null;
				} 
				
				if($db_cnf['port']) {
					$db_port = $db_cnf['port'];
				} else {
					$db_port = null;
				} 
	    		
	    		$db = dbconnect($db_host, $db_username, $db_password, null, $db_ssl, $db_port);
	    		if(!$db) {
	    			//No response from the master
	    			http_response_code(503);
					exit(0);
	    		
	    		}
	    		
	    		dbselect($db_name);
	  			db_set_charset('utf8mb4');
	  			db_misc();
	
	    	}
    	
    		return;
    	}

	function cc_mail($to_email, $subject, $body_text, $sender_email, $sender_name="", $to_name="", $bcc_email="")
	{
		global $root_server_url;
		global $local_server_path;
		global $notify;
		global $staging;
		global $process_parallel;
		global $cnf;
				
		if($notify == true) {		//This global variable may be set by an add-on e.g. the emailer, to ensure that notifications are not sent
			$cmd = 'nohup nice -n 10 ' . $cnf['phpPath'] .  ' ' . $local_server_path . 'send-email.php to=' . rawurlencode($to_email) . '  subject=' . rawurlencode($subject) . ' body=' . rawurlencode($body_text) . ' sender_email=' . rawurlencode($sender_email) .  ' sender_name=' . urlencode($sender_name) . ' to_name=' . urlencode($to_name) . ' staging=' . $staging . ' bcc=' . rawurlencode($bcc_email) . ' > /dev/null 2>&1 &';	//To log eg.: . ' >/var/www/html/atomjump_staging/tmp/newlog.txt';
									
			array_push($process_parallel, $cmd);        //Store to be run by index.php at the end of everything else.
		}

		//To test: https://atomjump.com/api/send-email.php?to=test@yourmail.com&subject=Test&body=test&sender_email=noreply@atomjump.com&sender_name=AtomJump

		return $result;
		
	}
	
	function send_generic_email($to_email, $subject, $body_text, $sender_email, $sender_name="", $to_name="", $bcc_email="")
	{
	 	global $cnf;
	 	global $local_server_path;
 
		require_once($local_server_path . "classes/PHPMailer/class.phpmailer.php");
		require_once($local_server_path . "classes/PHPMailer/class.smtp.php");

		$mail = new PHPMailer;

		//$mail->SMTPDebug = 3;                               // Enable verbose debug output

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = $cnf['email']['sending']['smtp'];  // Specify main and backup SMTP servers (with comma separator)
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $cnf['email']['sending']['user'];                 // SMTP username
		$mail->Password = $cnf['email']['sending']['pass'];                           // SMTP password
		if(isset($cnf['email']['sending']['encryption'])) {
			$mail->SMTPSecure = $cnf['email']['sending']['encryption'];                            // Enable TLS encryption, `ssl` also accepted
		}
		
		if(isset($cnf['email']['sending']['port'])) {
			$mail->Port = $cnf['email']['sending']['port'];
		} else {	
			$mail->Port = 587;                                    // TCP port to connect to
		}
	
		if((isset($sender_email))&&($sender_email != '')) {
			$mail->setFrom($sender_email, $sender_name);	//Note: some email senders will only send from a specific single email account e.g. the one set in noReplyEmail
		} else {
			$mail->setFrom($cnf['email']['noReplyEmail'], $cnf['email']['noReplyEmail']);
		}
		$mail->addAddress($to_email);     // Add a recipient
		
		if((isset($bcc_email))&&($bcc_email != '')) {			
			$mail->addBCC($bcc_email);
		}

		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body    = nl2br($body_text);
		$mail->AltBody = $body_text;
		$mail->CharSet = 'UTF-8';

		if(!$mail->send()) {
			error_log('Message could not be sent.');
			error_log('Mailer Error: ' . $mail->ErrorInfo);
		} else {
			//error_log('Message has been sent');
		}
		
		return;
	
	}
	
	
	
	function send_mailgun($to_email, $subject, $body_text, $sender_email, $sender_name="", $to_name="", $bcc_email="")
	{
	
	 	global $cnf;
 
		$config = array();
	 
		$config['api_key'] = $cnf['email']['sending']['vendor']['mailgun']['key']; 
		$config['api_url'] = $cnf['email']['sending']['vendor']['mailgun']['url'];
	 
		$message = array();
	 
		$message['from'] = $sender_email; 
		$message['to'] = $to_email;	
		if((isset($bcc_email))&&($bcc_email != '')) {
			$message['bcc'] = $bcc_email;
		}
		$message['subject'] = $subject;	 
		$message['html'] = nl2br($body_text); 
	
		$ch = curl_init();
	 
		curl_setopt($ch, CURLOPT_URL, $config['api_url']); 
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
	 
		$result = curl_exec($ch);
	 
		curl_close($ch);
	 
		return $result;
	 
	}
	

	function cc_mail_direct($to_email, $subject, $body_text, $sender_email, $sender_name="", $to_name="", $bcc_email="")
	{
		global $notify;			//This global variable may be set by an add-on e.g. the emailer, to ensure that notifications are not sent
		
				
		if($notify == true) {		
	
			if($cnf['email']['sending']['use'] == 'mailgun') {
		
				return send_mailgun($to_email, $subject, $body_text, $sender_email, $sender_name, $to_name, $bcc_email);
			} else {
				//A generic SMTP server
				return send_generic_email($to_email, $subject, $body_text, $sender_email, $sender_name, $to_name, $bcc_email);
			}
		}
		
		return false;
		
	}
	
	
	function cur_page_url() {
	
		if($_REQUEST['clientremoteurl']) {
			//Our passed in value
			return urldecode($_REQUEST['clientremoteurl']);
		} else {
			if($_REQUEST['remoteurl']) {
				//Our passed in value
				return urldecode($_REQUEST['remoteurl']);
			} else {
				 $pageURL = 'http';
				 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
					 $pageURL .= "://";
				 if ($_SERVER["SERVER_PORT"] != "80") {
				  	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
				 } else {
				  	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
				 }
				 return $pageURL;
			}
		}
	}
	
	define("ABOUT_LAYER_ID", 1);	
	define("DEFAULT_AD_WIDTH", 170);		//was 190
	
	function upload_to_all($filename, $raw_file, $specific_server = '', $target_path = "/images/property/")
	{
			//$filename is the full local file path e.g. ..../path-to-server/images/im/test/2/4/5/6/6/7/hello.jpg,
			//$raw_file is the 2/4/5/6/6/7/hello.jpg
			
			
			
			global $local_server_path;
			global $root_server_url;
			global $cnf;
			
			
			
			if($cnf['uploads']['use'] == "amazonAWS") {
				//Share to Amazon S3
				//Returns 'false' if image is not successfully put to Amazon, but otherwise 'true'
				//If this is a bucket-style file system, we will remove the subfolders from raw_file, to be just hello.jpg
				$raw_file = basename($raw_file);
	
				if(isset($cnf['uploads']['vendor']['amazonAWS']['uploadUseSSL'])) {
					$use_ssl = $cnf['uploads']['vendor']['amazonAWS']['uploadUseSSL'];
					
				} else {
					$use_ssl = false;		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['uploadEndPoint'])) {
					$endpoint = $cnf['uploads']['vendor']['amazonAWS']['uploadEndPoint'];
				} else {
					$endpoint = "https://s3.amazonaws.com";		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['bucket'])) {
					$bucket = $cnf['uploads']['vendor']['amazonAWS']['bucket'];
				} else {
					$bucket = "ajmp";		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['region'])) {
					$region = $cnf['uploads']['vendor']['amazonAWS']['region'];
				} else {
					$region = 'nyc3';
				}
				
								
				//Get an S3 client
				$s3 = new Aws\S3\S3Client([
						'version' => 'latest',
						'region'  => $region,				
						'endpoint' => $endpoint,			//E.g. 'https://nyc3.digitaloceanspaces.com'
						'credentials' => [
								'key'    => $cnf['uploads']['vendor']['amazonAWS']['accessKey'],
								'secret' => $cnf['uploads']['vendor']['amazonAWS']['secretKey'],
							]
				]);
				
	
				try {
					// Upload data.
					$result = $s3->putObject([
						'Bucket' => $bucket,
						'Key'    => $raw_file,
						'Body'   => file_get_contents($filename),
						'ContentType'  => 'image/jpeg',
						'ACL'    => 'public-read'
					]);

					// Print the URL to the object.
					error_log("Successfully uploaded: " . $result['ObjectURL']);
				} catch (S3Exception $e) {
					error_log($e->getMessage());
					return false;
				}
			
			}
			
			
			if($cnf['uploads']['use'] == "generic") {
				//Upload image to a generic remote service 
				$handle = fopen($filename, "r");
				$data = fread($handle, filesize($filename));
				//Note: Filename will include subfolders in it (and since these are in a POST request, no urlencoding is needed)
				$POST_DATA   = array('file'=>base64_encode($data),'FILENAME'=>$raw_file);
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $cnf['uploads']['genericUploadURL']);
				curl_setopt($curl, CURLOPT_TIMEOUT, 30);
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $POST_DATA);
				$response = curl_exec($curl);		        
				if(curl_error($curl))
				{
					error_log('error:' . curl_error($curl));
				}
			
				curl_close ($curl);
			}
		
		
			if(isset($cnf['uploads']['imagesShare'])) {
				//Share across our own servers
			
				//Get the domain of the web url, and replace with ip:1080
				$parse = parse_url($root_server_url);
				$domain = $parse['host'];
					
				if($specific_server == '') {  //Defaults to all
					$servers = array();
					for($cnt =0; $cnt< count($cnf['ips']); $cnt++) {
						$server_url = str_replace($domain, $cnf['ips'][$cnt] . ":" . $cnf['uploads']['imagesShare']['port'], $root_server_url) . "/copy-image.php";
						if($cnf['uploads']['imagesShare']['https'] == false) {
							//Only do with http
							$server_url = str_replace("https", "http", $server_url);
						}
						$servers[] = $server_url;
					
					}
				
				} else {
					//Only process this one server
					$servers = array($specific_server);
		
				}
				
	
				foreach($servers as $server)
				{
					//error_log("Sending to : " . $server);		//Include if testing
	
					//Coutesy http://stackoverflow.com/questions/19921906/moving-uploaded-image-to-another-server
					$handle = fopen($filename, "r");
					$data = fread($handle, filesize($filename));
					$POST_DATA   = array('file'=>base64_encode($data),'FILENAME'=>$raw_file, 'targetpath'=>$target_path);
					$curl = curl_init();
					curl_setopt($curl, CURLOPT_URL, $server);
					curl_setopt($curl, CURLOPT_TIMEOUT, 30);
					curl_setopt($curl, CURLOPT_POST, 1);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $POST_DATA);
					$response = curl_exec($curl);		        
					if(curl_error($curl))
					{
						error_log('error:' . curl_error($curl));
					}
				
					curl_close ($curl);
				
				
				 }
			 }
		     
		     return true;
		
	
	}
	
	function summary($details,$max)
	{
		if(strlen($details)>$max)
		{
		    $details = substr($details,0,$max);
		    $i = strrpos($details," ");
		    $details = substr($details,0,$i);
		    $details = $details."...";
		}
		return $details;
	}
	
	
	function show_translated_number($number, $lang)
	{
		//Input, ideally an integer between 0 - 60 (60 seconds in a minute means this
		//is usually the largest number we need for most things, i.e. 59 seconds ago). 
		//However, any number can be used and the default is to return the English number
		//if there is no matching number in the messages array "number" conversion for the
		//input language.
		global $msg;
				
		if($msg['msgs'][$lang]['numbers']) {
			//Yes the numbers array exists for this language
			//Check definitely an integer
			if(is_int(intval($number))) {
				if($msg['msgs'][$lang]['numbers'][$number]) {
					//Return the string if this is in the 'number' array as the indexed version of that number	
					return $msg['msgs'][$lang]['numbers'][$number];
				} else {
					return $number;				
				}			
			} else {
				return $number;
			}
		} else {
			return $number;
		}
		
	
	}
	
	function web_root($cnf)
	{
		//Get a home API url for this service
		//Note: for a human link to this service use 
		/* $follow_on_link = "https://atomjump.com";
			if($cnf['serviceHome']) {
				$follow_on_link = add_subdomain_to_path($cnf['serviceHome']);
			}
		*/
		$webroot = trim_trailing_slash($cnf['webRoot']);
		
		//Get the web homepage
		$subdomain = check_subdomain();
 			
		//Substitute [subdomain] with the actual subdomain if allowed and it exists
		if((isset($cnf['readURLAllowReplacement'])) && ($cnf['readURLAllowReplacement'] == true)) {
			if((isset($cnf['webrootIncludeDot'])) && ($cnf['webrootIncludeDot'] == true)) {
				$webroot = trim_trailing_slash(str_replace('[subdomain]', $subdomain . ".", $webroot));
			} else {
				$webroot = trim_trailing_slash(str_replace('[subdomain]', $subdomain , $webroot));
			}
		} else {
			$webroot = trim_trailing_slash(str_replace('[subdomain]', "", $webroot));		//Remove any mention of subdomains
		}
		
		return $webroot;
	
	}
	
	function read_url($cnf)
	{
		//Support a specific URL for fast reading with the loop-server-fast plugin 
		//Similar to web_root(), above, but is only used for a reading url address, not a link
		$read_url = null;
		
		if(isset($cnf['readURL'])) {
			//This may be used for modifying the fast server web address
			$subdomain = check_subdomain();

			if((isset($cnf['readURLAllowReplacement'])) && ($cnf['readURLAllowReplacement'] == true)) {
				if((isset($cnf['readURLIncludeDot'])) && ($cnf['readURLIncludeDot'] == true)) {
					$read_url = trim_trailing_slash(str_replace('[subdomain]', $subdomain . ".", $cnf['readURL']));
				} else {
					$read_url = trim_trailing_slash(str_replace('[subdomain]', $subdomain , $cnf['readURL']));
				}
			} else {
				$read_url = trim_trailing_slash(str_replace('[subdomain]', "", $cnf['readURL']));		//Remove any mention of subdomains
			}
		}
			
		return $read_url;			
			
	}	
	
	
	
	function dbconnect($host, $user, $pass, $dbname = null, $ssldetails = null, $dbport = 3306)
	{
		//Using old style:
		/*
			mysql_connect($host, $user, $pass);
		*/
	
		
		if(($ssldetails) && (isset($ssldetails['use'])) && ($ssldetails['use'] === true)) {
			//SSL connection
			$con = mysqli_init();
			if (!$con) return false;
			
			mysqli_ssl_set($con, $ssldetails['key'], $ssldetails['cert'], $ssldetails['cacert'], $ssldetails['capath'], $ssldetails['protocol']);   
						
			if(isset($ssldetails['verify']) && ($ssldetails['verify'] === false)) {
				$connection_type = MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT;   //  works for PHP >= 5.6
			} else {
				$connection_type = MYSQLI_CLIENT_SSL;			
			}
						
						
			if($dbname) {
				if(mysqli_real_connect($con,"p:" . $host, $user, $pass, $dbname, $dbport, null, $connection_type)) {  
			        return $con;
			    } else {
			    	return false;
			    }
			} else {
				if(mysqli_real_connect($con,"p:" . $host, $user, $pass, null, $dbport, null, $connection_type)) { 
					return $con;
				} else {
					return false;
				}
			}
			
		} else {
		
			//Normal non-ssl
		
			if($dbname) {
				return mysqli_connect("p:" . $host, $user, $pass, $dbname, $dbport);		//p is for persistent connection
			} else {
				return mysqli_connect("p:" . $host, $user, $pass, null, $dbport);
			}
		}
		
	}
	
	function dbselect($dbname)
	{
		global $db;
		//Using old style = mysql_select_db($dbname);
		return mysqli_select_db($db, $dbname);
		
	}
	
	function dbclose()
	{
		global $db;
		//Using old style = mysql_close();
		mysqli_close($db);
		
	}
	
	function create_query_sql_string($sql, $params) {
		//For test display purposes only
		$replaced_sql = $sql;
		$typ = 0;
		$val = 1;
			
		for($cnt = 0; $cnt < sizeof($params); $cnt++) {
				
			if($params[$cnt][$typ] == "i") $sql_string = $params[$cnt][$val];
			if($params[$cnt][$typ] == "s") $sql_string = "'" . $params[$cnt][$val] . "'";
			if($params[$cnt][$val] == null) $sql_string = "NULL";
			$replaced_sql = preg_replace('/\?/', $sql_string, $replaced_sql, 1);
		}
		return $replaced_sql;
	}
	
	function dbquery($sql, $params = null, $error_log_sql = null, $use = MYSQLI_STORE_RESULT, $die = false)
	{
		//Input examples:
		//A basic, non parametized query
		//$results = dbquery("SELECT * FROM test WHERE id = 10 AND string = 'test'");					
		//
		//The params include "i" for integer and "s" for string.		
		//$results = dbquery("SELECT * FROM test WHERE id = ? AND string = ?", [["i", $id], ["s", $string]]);				
		//
		//This displays the query in it's completed string form to error_log for debugging purposes. 
		//Once it has been debugged, remove the 3rd "TEST" param
		//$results = dbquery("SELECT * FROM test WHERE id = ? AND string = ?", [["i", $id], ["s", $string]], "TEST");		
	
		global $db;
		
		if($params) {
			//Parametized query
			$typ = 0;
			$val = 1;
			
			if($error_log_sql) {
				//Loop through and create a string of the query			
				error_log(create_query_sql_string($sql, $params));
			}
						
			if($stmt = mysqli_prepare($db, $sql)) {
			
			
				//Example: $stmt->bind_param("iissiii", $layer, $last_id, $ip, $ip, $logged_user, $logged_user, $initial_records);
				switch(sizeof($params)) {
					case 1:
						$stmt->bind_param($params[0][$typ], $params[0][$val]); 
					break;
					
					case 2:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ],
											 $params[0][$val], $params[1][$val]); 
					break;
					
					case 3:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val]); 
					break;
					
					case 4:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val]); 
					break;
					
					case 5:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val]); 
					break;
					
					case 6:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val]); 
					break;
					
					case 7:
						$str = $params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ];									  
						$stmt->bind_param($str,
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val]); 
					break;
					
					case 8:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val]); 
					break;
					
					case 9:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ] . $params[8][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val], $params[8][$val]);
					break;
					
					case 10:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ] . $params[8][$typ] . $params[9][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val], $params[8][$val], $params[9][$val]);
					break;
					
					case 11:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ] . $params[8][$typ] . $params[9][$typ] .
										  $params[10][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val], $params[8][$val], $params[9][$val],
											 $params[10][$val]);
					break;
					
					case 12:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ] . $params[8][$typ] . $params[9][$typ] .
										  $params[10][$typ] . $params[11][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val], $params[8][$val], $params[9][$val],
											 $params[10][$val], $params[11][$val]);
					break;
					
					case 13:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ] . $params[8][$typ] . $params[9][$typ] .
										  $params[10][$typ] . $params[11][$typ] . $params[12][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val], $params[8][$val], $params[9][$val],
											 $params[10][$val], $params[11][$val], $params[12][$val]);
					break;
					
					case 14:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ] . $params[8][$typ] . $params[9][$typ] .
										  $params[10][$typ] . $params[11][$typ] . $params[12][$typ] . $params[13][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val], $params[8][$val], $params[9][$val],
											 $params[10][$val], $params[11][$val], $params[12][$val], $params[13][$val]);
					break;
					
					case 15:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ] . $params[8][$typ] . $params[9][$typ] .
										  $params[10][$typ] . $params[11][$typ] . $params[12][$typ] . $params[13][$typ] . $params[14][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val], $params[8][$val], $params[9][$val],
											 $params[10][$val], $params[11][$val], $params[12][$val], $params[13][$val], $params[14][$val]);
					break;
					
					case 16:
						$stmt->bind_param($params[0][$typ] . $params[1][$typ] . $params[2][$typ] . $params[3][$typ] . $params[4][$typ] .
										  $params[5][$typ] . $params[6][$typ] . $params[7][$typ] . $params[8][$typ] . $params[9][$typ] .
										  $params[10][$typ] . $params[11][$typ] . $params[12][$typ] . $params[13][$typ] . $params[14][$typ] .
										  $params[15][$typ],
											 $params[0][$val], $params[1][$val], $params[2][$val], $params[3][$val], $params[4][$val],
											 $params[5][$val], $params[6][$val], $params[7][$val], $params[8][$val], $params[9][$val],
											 $params[10][$val], $params[11][$val], $params[12][$val], $params[13][$val], $params[14][$val],
											 $params[15][$val]);
					break;
					
					default:
						//Really shouldn't be here
						$stmt->bind_param($params[0][$typ], $params[0][$val]); 
					break;
				}
				
				//By default, we fall through correctly
				$output = new stdClass();
				$output->result = true;
				
				try {
					if (!$stmt->execute()) {
						//Something has gone wrong
						error_log("Execute query failed:" . create_query_sql_string($sql, $params));
						error_log("Failed query params: " . json_encode($params));
						error_log("Failed query sql: " . $sql);
						return false;
					}
					
					
					$result = $stmt->get_result();
					
					$data = new stdClass();
					$data->result = [];
					if($result) {
						while ($myrow = $result->fetch_assoc()) {
							$data->result[] = $myrow;
						}
						
						if(empty($data)) {
							//Set some info, as the standard Mysql responds with, and prevents a false 'die' response
							$data->result = [ [] ];	//PHP mysqli scripts respond with:  {"current_field":null,"field_count":null,"lengths":null,"num_rows":null,"type":null};
						}
						$output = $data;
					} else {
						//Leave $output as true. Likely an INSERT or UPDATE query.
					}

					$output->affected_rows = $stmt->affected_rows;	//Get the number of affected rows

					$stmt->free_result();
					
					$stmt->close();			//Not sure if this needs to remain open also
					
					
		
					
				} catch (Exception $e) {
					error_log($e->getMessage());
					$stmt->close();	
					return false;
				}
				
				return $output;
			} else {
				error_log("Could not prepare statement " . $sql); 	
				return false;
			}
			
		} else {
			//A basic query with no parameters.
			
			//old style: return mysql_query($sql);
			if($die == true) {
				return mysqli_query($db, $sql, $use) or die("Unable to execute query $sql " . dberror());
			} else {
				return mysqli_query($db, $sql, $use);
			}
		}
	}
	
	function db_affected_rows($result = null)
	{
		global $db;
		if($result) {
			return $result->affected_rows;
		} else {
			return mysqli_affected_rows($db);
		}
	
	}
	
	function db_fetch_array(&$result, $params = null)
	{	
		//The pass-by reference for the result allows the array to decrease in size in the params case each time this func is called.
	
		if(is_a($result, 'mysqli_result')) {
			//A normal mysqli result		
			
			//Old style: mysql_fetch_array($result)
			return mysqli_fetch_array($result);	
		} else {
			//Get directly from the result as an array - usually a result from a parameterized query
			$result_row = array_shift($result->result);		//Reduce the array by one element
			if($result_row) {
				return $result_row;
			} else {
				return false;
			}
		}
	}
	
	function db_real_escape_string($str)
	{
		global $db;
		//Using old style: mysql_real_escape_string
		return mysqli_real_escape_string($db, $str);
		//Caution character set must be set prior with mysqli_set_charset() 
		
	}
	
	function dberror()
	{
		global $db;
		//Old style: mysql_error()
		return mysqli_error($db);
	}
	
	function db_insert_id()
	{
		global $db;
		//Old style: mysql_insert_id();
		return mysqli_insert_id($db);
	}

	function db_set_charset($set)
	{
		global $db;
		//Old way: dbquery("SET NAMES 'utf8'");
		mysqli_set_charset($db, $set);
	}
	
	function db_misc()
	{
		
		//Old way would have nothing in here. mysqli needs it for innodb tables - we have a few.
		dbquery('SET AUTOCOMMIT = 1');	
	
	}
	
	function db_active_connections()
	{
		global $db;
		
		//Note: these only work on a connection basis
		//[active_connections] => 1
    	//[active_persistent_connections] => 0
		//$stats = mysqli_get_connection_stats($db);
		//$total_connections = $stats['active_connections'];
		
		//This works globally on the server, provided you are have rights to it.
		$total_connections = 1;		//Default to something - this won't really check, but at least it will return a number. It should always be less than the default total allowed (140).
		$result = dbquery("SHOW STATUS WHERE `variable_name` = 'Threads_connected'");
		while($row = db_fetch_array($result)) {
			$total_connections = $row['Value'];
		}
		return $total_connections;
	}

?>
