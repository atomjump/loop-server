<?php
	$uploaded = false;
	$outgoing_type = null;
	$file_in_place = false;
	$thumbnail = false;
	
	global $msg;
	global $cnf;
	global $lang;
	if(!$lang) $lang = $msg['defaultLanguage'];
	if(!$lang) $lang = "en";		//default in case there is no definition
	
	if(!$subfolders) {
		//$_REQUEST['title'] should be something like 3/5/2/6/3/6/filename, and output_folders will become 3/5/2/6/3/6/
		$subfolders = dirname($_REQUEST['title']);
	}

	foreach($_FILES as $file) {
	 
		//Upload an image
		global $local_server_path;
		$target_dir = getcwd() . $image_path; //"/../images/property/";
		
		$imageFileType = $file["type"];	// pathinfo($file["tmp_name"]["type"],PATHINFO_EXTENSION);		//This isn't the best because it is browser reporting it but it hopefully removes non jpg
		$uploaded_ok = true;
		
		// Check file size
		if ($file["size"] > 10000000) {
			$message = $msg['msgs'][$lang]['photos']['largeError'];
			$uploaded_ok = false;
		} else {
		
			switch($imageFileType) {
			
				case "video/webm":
					//A video uploaded using browser-based camera input
					
					$raw_webm_file = $_REQUEST['title'] . ".webm";
					$webm_file = $target_dir . $raw_webm_file;
					
					$raw_mp4_file = $_REQUEST['title'] . ".mp4";
					$raw_file = $raw_mp4_file;
					$target_file = $target_dir . $raw_mp4_file;
					
					$raw_thumb = $_REQUEST['title'] . ".jpg";
					$thumb_file = $target_dir . $raw_thumb;
					
					
					$output_folders = $target_dir . $subfolders;
					if(!is_dir($output_folders)) { 
						mkdir($output_folders, 0777, true); 
					}
					
					if (move_uploaded_file($file["tmp_name"], $webm_file)) {
						//Convert webm file into .mp4 file
						
						//Note: requires "sudo apt install ffmpeg"
						$response = shell_exec("ffmpeg -i " . escapeshellarg($webm_file) . " -movflags faststart -fpsmax 30 " . escapeshellarg($target_file));		//Run the conversion to mp4
						$outgoing_type = "mp4";
						
						if((file_exists($target_file)) && (filesize($target_file) > 0)) {
							$file_in_place = true;
						} else {
							$message .= "&nbsp;" . $msg['msgs'][$lang]['photos']['generalError'];   //"Sorry, there was an error converting your file.";
						}
						
						//Remove the original webm file, regardless
						unlink($webm_file);
					} else {
						//File not moved correctly
						$message .= "&nbsp;" . $msg['msgs'][$lang]['photos']['generalError'];	//"Sorry, there was an error uploading your file.";
					}
					
					//Now create a thumbnail .jpg of the video file.
					$response = shell_exec("ffmpeg -i " . escapeshellarg($target_file) . " -ss 00:00:00.501 -frames:v 1 " . escapeshellarg($thumb_file));		//Trying .5 seconds in so that colours have balanced
					if(!file_exists($thumb_file)) {
						//Do the very 1st frame
						$response = shell_exec("ffmpeg -i " . escapeshellarg($target_file) . " -ss 00:00:00.000 -frames:v 1 " . escapeshellarg($thumb_file));		//Trying 1st frame
					}
					
					if(file_exists($thumb_file)) {
						$thumbnail = true;
					} 
					
					$media_type	= "video";
					
					
				break;
				
				case "audio/mp3":
					//An audio file uploaded using browser-based audio input.			
					$type_wav = false;
					$type_ogx = false;
					
					$raw_mp3_file = $_REQUEST['title'] . ".mp3";
					$raw_file = $raw_mp3_file;
					$target_file = $target_dir . $raw_mp3_file;
					
					$raw_ogx_file = $_REQUEST['title'] . ".ogx";
					$ogx_file = $target_dir . $raw_ogx_file;
					
					$raw_wav_file = $_REQUEST['title'] . ".wav";
					$wav_file = $target_dir . $raw_wav_file;
										
					//If firefox, it will have sent through an .ogx file
					
					
					if(strstr($file["name"], ".ogx")) {
						$temporary_file = $ogx_file;
						$type_ogx = true;
					} else {
						if(strstr($file["name"], ".wav")) {			//This is likely an iphone
							$temporary_file = $wav_file;
							$type_wav = true;
						} else {
							//All the other browsers upload an .mp3
							$temporary_file = $target_file;
							$type_ogx = false;
						}
					}
					
					$output_folders = $target_dir . $subfolders;
					if(!is_dir($output_folders)) { 
						mkdir($output_folders, 0777, true); 
					}
					
					if (move_uploaded_file($file["tmp_name"], $temporary_file)) {
						
						$outgoing_type = "mp3";
						
						if($type_ogx == true) {
							//Need to convert into an .mp3
							
							//Note: requires "sudo apt install ffmpeg"
							$response = shell_exec("ffmpeg -i " . escapeshellarg($ogx_file) . " " . escapeshellarg($target_file));		//Run the conversion to mp3
							
							
							if((file_exists($target_file)) && (filesize($target_file) > 0)) {
								$file_in_place = true;
							} else {
								$message .= "&nbsp;" . $msg['msgs'][$lang]['photos']['generalError'];	//"Sorry, there was an error converting your file.";
							}
							
							//Remove the original webm file, regardless
							unlink($ogx_file);
						} else {
							if($type_wav == true) {
								//Need to convert into an .mp3
							
								//Note: requires "sudo apt install ffmpeg"
								$response = shell_exec("ffmpeg -i " . escapeshellarg($wav_file) . " " . escapeshellarg($target_file));		//Run the conversion to mp3
							
							
								if((file_exists($target_file)) && (filesize($target_file) > 0)) {
									$file_in_place = true;
								} else {
									$message .= "&nbsp;" . $msg['msgs'][$lang]['photos']['generalError'];		//"Sorry, there was an error converting your file.";
								}
								
								//Remove the original webm file, regardless
								unlink($wav_file);
							
							} else {
						
								//Normal .mp3, leave as-is
								$file_in_place = true;
							}
						}
					} else {
						//File not moved correctly
						$message .= "&nbsp;" . $msg['msgs'][$lang]['photos']['generalError'];	//"Sorry, there was an error uploading your file.";
					}
					
					$media_type	= "audio";
					
				
				break;
			
				default:	
					//Image upload case
					$raw_file = $_REQUEST['title'] . ".jpg";
					$hi_raw_file = $_REQUEST['title'] . "_HI.jpg";		//Hi res version
					$target_file = $target_dir . $raw_file;
					$hi_target_file = $target_dir . $hi_raw_file;
					
					
					// Check if image file is a actual image or fake image
					if(isset($_POST["submit"])) {
						$check = getimagesize($file["tmp_name"]);
						if($check !== false) {
							$uploaded_ok = true;
						} else {
							$message = $msg['msgs'][$lang]['photos']['notImage'];	//"File is not an image. ";
							$uploaded_ok = false;
						}
					}
					
					
					// Allow certain file formats
					if((stristr($imageFileType, "jpg") == false) && (stristr($imageFileType, "jpeg") == false)) {
						$message = $msg['msgs'][$lang]['photos']['notImage'];		//"Sorry, only JPG or JPEG files are allowed.";
						$uploaded_ok = false;
					}
					// Check if $uploaded_ok is set to false by an error
					if ($uploaded_ok == false) {
						$message .= $msg['msgs'][$lang]['photos']['generalError'];	//"&nbsp;Your file was not uploaded.";
						// if everything is ok, try to upload file
					} else {
					
						$output_folders = $target_dir . $subfolders;
						if(!is_dir($output_folders)) { 
							mkdir($output_folders, 0777, true); 
						}
					
						if (move_uploaded_file($file["tmp_name"], $target_file)) {
						
							$message .= "&nbsp; The file ". basename( $file["name"]). " has been uploaded.";		//I don't think this ever gets displayed but is useful for debugging the JSON
							
							//Resize the image
							$src = imagecreatefromjpeg($target_file);        
							list($width, $height) = getimagesize($target_file); 

							$ratio = $height / $width;
							$resize = true;
								
							$base_size = 800;		//For a roughly proportioned 800x450 image
							
							if($height > $width) {
								$base_size = 450;
							}
							
							if($resize == true) {
								$tmp = imagecreatetruecolor($base_size, ($base_size*$ratio));
								$filename = $target_file;

								imagecopyresampled($tmp, $src, 0, 0, 0, 0, $base_size, ($base_size*$ratio), $width, $height); 
								imagejpeg($tmp, $filename, 75);		//75% quality - this saves a lot on download space
								
								$hi_res = true;
								//We want a hi res version too
								$base_size = 1280;		//For a roughly proportioned 1280x720 image
								
								if($height > $width) {
									$base_size = 720;
								}
								
								$tmp = imagecreatetruecolor($base_size, ($base_size*$ratio));
								$filename = $hi_target_file;

								imagecopyresampled($tmp, $src, 0, 0, 0, 0, $base_size, ($base_size*$ratio), $width, $height); 
								imagejpeg($tmp, $filename, 95);		//95% quality - this produces a good quality image for slower secondary downloads	
								
							}
							
							//Image resized
							$file_in_place = true;
							
							
							

							
						} else {
							//File not moved correctly
							error_log("Warning: there was a problem moving the file " . $file["tmp_name"] . " to " . $target_file . " during an upload attempt");
							$message = $msg['msgs'][$lang]['photos']['generalError'];	//Sorry, there was an error uploading your file.";
						}
					}
					
					$outgoing_type = "jpg";
					
					$media_type	= "image";
				 break;  //end of if an image to upload
			} //end of switch
		}   //end of filesize over
	}	//Shouldn't this be here?
	
	
	
	if($file_in_place == true) {
		//So the file is in place and ready for consumption on this server.
		
		//Now copy across to the other
		//also see: upload_to_all($filename, $raw_file);		//Works! Just testing the follow in a parrallel process
		
		//Copy across to the other servers for future reference - but do in a separate process

		global $local_server_path;
		global $cnf;
		
		if(!isset($images_script)) {
			$script = $local_server_path . "back-end/send-images.phd";
		
		} else {
			$script = $images_script;
		
		}
		$cmd = $cnf['phpPath'] . ' ' . $script . ' ' . escapeshellarg($raw_file);
		error_log("Running " . $cmd);
		
		$response = shell_exec($cmd);
		
		//In the images case, there is a hi-res image, also
		if($hi_res == true) {
			$cmd = $cnf['phpPath'] . ' ' . $script . ' ' . escapeshellarg($hi_raw_file);
			error_log("Running " . $cmd);
			
			$response = shell_exec($cmd);
		}
		
		//In the video upload case, there is an additional .jpg file for a thumbnail that needs to be shared, also.
		if($thumbnail == true) {
			$cmd = $cnf['phpPath'] . ' ' . $script . ' ' . escapeshellarg($raw_thumb);
			error_log("Running " . $cmd);
			
			$response = shell_exec($cmd);
		}
		
		if($response == '') {
			//A blank response signals all clear
			$uploaded = true;
		} else {
			$uploaded = false;
			error_log("Warning: there was a problem sharing the media file in the command: " . $cmd);
			$message = $msg['msgs'][$lang]['photos']['generalError'];		//"&nbsp;Sorry, there was an error uploading your file";
		
		}
	}
	 
	 
?>
