<?php 

 //Internal frame that appears within the popup
	require('config/db_connect.php');
	require("classes/cls.layer.php");
	require("classes/cls.ssshout.php");
	
	$ly = new cls_layer();
	$sh = new cls_ssshout();
	
	
	$inner_type = "search-secure";
	include("components/inner_js_filename.php");
	
	$warning = "";			//No warning, by default

	
	//We may have a possible user request in the case of receiving an email
	if(isset($_REQUEST['possible_user'])) {
	
		if(isset($_REQUEST['check'])) {
			if($sh->check_email_secure(urldecode($_COOKIE['email']), $_REQUEST['check'])) {
				//Test if there is no password on this email account, and set ourselves as the logged in user
				$sh->new_user($_COOKIE['email'], '');
				$_SESSION['logged-email'] = urldecode($_COOKIE['email']);
			}
		}
	
	}
	
	$read_url = read_url($cnf);

	
	function currentdir($url) {
		// note: anything without a scheme ("example.com", "example.com:80/", etc.) is a folder
		// remove query (protection against "?url=http://example.com/")
		if ($first_query = strpos($url, '?')) $url = substr($url, 0, $first_query);
		// remove fragment (protection against "#http://example.com/")
		if ($first_fragment = strpos($url, '#')) $url = substr($url, 0, $first_fragment);
		// folder only
		$last_slash = strrpos($url, '/');
		if (!$last_slash) {
			return '/';
		}
		// add ending slash to "http://example.com"
		if (($first_colon = strpos($url, '://')) !== false && $first_colon + 2 == $last_slash) {
			return $url . '/';
		}
		return substr($url, 0, $last_slash + 1);
	}

	
	function urldir($relativeurl, $callerurl) {
	  return currentdir($callerurl) . $relativeurl;
	  //reduce callerurl from https://blahblah/dir/dir/script.xyz
	  // to https://blahblah/dir/dir/
	  // and then add a relative url to it
	  // eg. '../../dir/script.css'
	}
	
	//Optional params
	if(isset($_REQUEST['server'])) {
	    $server = clean_output($_REQUEST['server']);
	} else {
	    $server = "https://atomjump.com";
	}
	if(isset($_REQUEST['clientremoteurl'])) {
	    $clientremoteurl = clean_output($_REQUEST['clientremoteurl']);
	} else {
	    $clientremoteurl = "https://atomjump.com/index.html";
	}
	
	
	
	
	
	if((isset($_REQUEST['cssBootstrap']))&&($_REQUEST['cssBootstrap'] != '')) {
	    
	    if(substr($_REQUEST['cssBootstrap'], 0, 4) == "http") {
	        //An absolute url
	        $cssBootstrap = clean_output($_REQUEST['cssBootstrap']);
	    } else {
	        //A relative one
	        $cssBootstrap = urldir(clean_output($_REQUEST['cssBootstrap']), $clientremoteurl);
	    }
	    //TODO: Possible improvement here - check for https/http of server = https/http of css files
	} else {
	    $cssBootstrap = "https://atomjump.com/css/bootstrap.min.css";
	}
			
    if((isset($_REQUEST['cssFeedback']))&&($_REQUEST['cssFeedback'] != '')) {
	    if(substr($_REQUEST['cssFeedback'], 0, 4) == "http") {
	         //An absolute url
	        $cssFeedback = clean_output($_REQUEST['cssFeedback']);
	    } else {
	        //A relative one
	        $cssFeedback = urldir(clean_output($_REQUEST['cssFeedback']), $clientremoteurl);
	    }    
	        
	        
	} else {
		if($staging == true) {
			$cssFeedback = "https://staging.atomjump.com/css/comments-0.1.css";
		} else {
	    	$cssFeedback = "https://atomjump.com/css/comments-0.1.css";
	    }
	}
		
	//Get the layer info into the session vars
	$layer_info = $ly->get_layer_id($_REQUEST['uniqueFeedbackId'], null);
	if(isset($layer_info['var_public_code'])) {
		$granted = false;
		
		if(($_SESSION['access-layer-granted'] == $layer_info['int_layer_id'])||($ly->is_layer_granted($layer_info['int_layer_id']))) { 	//Normal access has been granted  
			$granted = true;
		}
    } else {
    
    	//A new passcode  - we need to create a new layer for a correct timestamp on the video link.
		$layer_info = array();
		$layer = $ly->new_layer($_REQUEST['uniqueFeedbackId'], 'public');
    	$layer_info = $ly->get_layer_id($_REQUEST['uniqueFeedbackId'], null);
    
    
    	$granted = true;
    }
	//Get new user in here, and set user IP address in session
	
	//Keep track of the number of views we have from this session - also reset if reloading
	$_SESSION['view-count'] = 0;
	
	
	//Check if we are subscribed.
	$lg = new cls_login();
	//Standard setup
	$subscribe_text = "subscription";
	if($msg['msgs'][$lang]['subscription']) $subscribe_text = $msg['msgs'][$lang]['subscription'];
	$subscribe = "<a href=\"javascript:\" onclick=\"$('#email-explain').slideToggle(); $('#save-button').html('" . $msg['msgs'][$lang]['subscribeSettingsButton'] . "')\" title=\"" . $msg['msgs'][$lang]['yourEmailReason'] . "\">" . $subscribe_text . "</a>";
	$subscribe_toggle_pic_no_ear = "<img src=\"" . $root_server_url . "/images/no-ear.svg\" title=\"Subscribe\" style=\"width: 32px; height: 32px;\">";
	$subscribe_toggle_pic_ear = "<img src=\"" . $root_server_url . "/images/ear.svg\" title=\"Unsubscribe\" style=\"width: 32px; height: 32px;\">";
	
	if($_SESSION['logged-user']) {
		$logged_user_text = $_SESSION['logged-user'];
	} else {
		$logged_user_text = "null";
	}

	$subscribe_toggle_no_ear = "<a href=\"javascript:\" onclick=\"return subFront(" . $logged_user_text . ",\'" . clean_output($_REQUEST['uniqueFeedbackId']). "\');\" title=\"" . $msg['msgs'][$lang]['yourEmailReason'] . "\">" . $subscribe_toggle_pic_no_ear . "</a>";
	$subscribe_toggle_ear = "<a href=\"javascript:\" onclick=\"return unSub(" . $logged_user_text . ",\'" . clean_output($_REQUEST['uniqueFeedbackId']) . "\');\" title=\"" . $msg['msgs'][$lang]['yourEmailReason'] . "\">" . $subscribe_toggle_pic_ear . "</a>";
	$subscribe_toggle = stripslashes($subscribe_toggle_no_ear);
	

	if($_SESSION['logged-email']) {
		//We are logged in, but not a forum owner
		//Not subscribed. Show a subscribe link.
		$subscribe_text = "subscribe";
		$subscribe = "<a href=\"javascript:\" onclick=\"return subFront(" . $logged_user_text . ",'" . clean_output($_REQUEST['uniqueFeedbackId']) . "');\" title=\"" . $msg['msgs'][$lang]['yourEmailReason'] . "\">" . $subscribe_text . "</a>";
		
		$subscribe_toggle = stripslashes($subscribe_toggle_no_ear);
	}

	$validation_code = "invalid";		//By default we can't unsub
	$validation_code = $lg->get_validation_code($logged_user_text);
	
	if(($layer_info)&&($_SESSION['logged-user'] != "")) {
	    				
		//Only the owners can do this
		$isowner = $lg->is_owner($logged_user_text, $layer_info['int_group_id'], $layer_info['int_layer_id']);
		if($isowner == true) {	
			//Subscribed already. Show an unsubscribe link
			$subscribe_text = "unsubscribe";
			if($msg['msgs'][$lang]['unsubscribe']) $subscribe_text = $msg['msgs'][$lang]['unsubscribe'];
			$subscribe = "<a href=\"javascript:\" onclick=\"return unSub(" . $logged_user_text . ",'" . clean_output($_REQUEST['uniqueFeedbackId']) . "');\" title=\"" . $msg['msgs'][$lang]['yourEmailReason'] . "\">" . $subscribe_text . "</a>";
			$subscribe_toggle = stripslashes($subscribe_toggle_ear);
			
		}
		
		
	}
		
	if(isset($layer_info['passcode'])) {
		//There is a layer already
		if($layer_info['date_owner_start']) {
			$date_start = date("Y-m-d H:i:s", strtotime($layer_info['date_owner_start']));
			$video_code = strtotime($layer_info['date_owner_start']);
		} else {
			//It was not created at the start (i.e. a very old forum). Use part of the passcode lettering itself.
			$video_code = substr($layer_info['passcode'], -7);
			
		}
		
		if(isset($layer_info['int_video_server'])) {
			$video_server = $layer_info['int_video_server'];
		} else {
			$video_server = 0;
			$ly->set_layer_video($_REQUEST['uniqueFeedbackId'], $video_server); 
		}
	} else {
		//No existing layer
		if($layer_info['date_owner_start']) {
			$date_start = date("Y-m-d H:i:s", strtotime($layer_info['date_owner_start']));
		} else {
			//WARNING: don't use a video_code here. It causes a bug. We should never get here.
			// It needs to match up with what other users are seeing from this same forum, generated off the database entry.
			$date_start = "ERROR";	//date("Y-m-d H:i:s");
		}
		$video_code = strtotime($date_start);
		
		//Set the video server to use
		$total = $cnf['video']['totalVideoServers'];
		$video_server = rand(1, $total) - 1;		//Starting from 0
		$ly->set_layer_video($_REQUEST['uniqueFeedbackId'], $video_server); 
	}
	
	
	
	
	
	
			
	//Check if we have too many database connections (i.e. too many concurrent users), and respond with a 'please come back later' sort of message.
	global $db_cnf;
	if(isset($db_cnf['maxConcurrentUsers'])) {
		$max_connections = $db_cnf['maxConcurrentUsers'];
	} else {
		$max_connections = 140;		//For a standard single node PHP-only setup. Typically MySQL allows for 150 concurrent connections.
	}
	if(isset($db_cnf['warningConcurrentUsers'])) {
		$warning_connections = $db_cnf['warningConcurrentUsers'];
	} else {
		$warning_connections = 100;		//For a standard single node PHP-only setup. Slightly less than the maxConcurrentUsers above
	}
	$current_connections = db_active_connections();
	
	
	$within_capacity_file = __DIR__ . '/images/im/capacity/within-db-capacity-warning.html';
	if($current_connections > $warning_connections) {
		//Over the warning level threshold of connections. Note: we make use of the image writable folder here.
		
		if(file_exists($within_capacity_file)) {
			//Remove the warning file, so that a remote service can see we are not within capacity, 
			//by the file's external lack of response from e.g. https://your.service.com/api/images/im/within-capacity-warning.html.
			unlink($within_capacity_file);
		}
		
	} else {
		//Under the warning level threshold
		if(file_exists($within_capacity_file)) {
			//No need to do anything, it already exists, so we know we're within capacity
		} else {
			//Create the file again
			$within_capacity_folder = __DIR__ . "/images/im/capacity/";
			if (!file_exists($within_capacity_folder)) {
				mkdir($within_capacity_folder);
			}
			$handle = fopen($within_capacity_file, 'w');			
			if(!$handle) {
				 error_log("Cannot create capacity warning file:  " . $within_capacity_file .
								 "   Please make sure your /images/im/capacity folder is writable by your public web server user.");
			} else {
				$data = '<html><body>You messaging server is within capacity. If this file disappears, you have crossed the warning level on server messaging capacity.</body></html>';
				fwrite($handle, $data);
			}
		}
	}
	
	if($current_connections > $max_connections) {
		
		if(isset($msg['msgs'][$lang]['tooManyConcurrentUsers'])) {
			$too_many_users = $msg['msgs'][$lang]['tooManyConcurrentUsers'];
		} else {
			$too_many_users = "Sorry, there are too many people trying to use this forum at once. Please come back and try again later.";
		}
		
		?>
		<!DOCTYPE html>
		<html lang="en">
		  <head>
				<meta charset="utf-8">
				 <title>AtomJump Loop - messaging for your site</title>
		 
				 <meta name="description" content="<?php echo $msg['msgs'][$lang]['description'] ?>">
		 
				 <meta name="keywords" content="<?php echo $msg['msgs'][$lang]['keywords'] ?>">
		 
					  <!-- Bootstrap core CSS -->
					<link rel="StyleSheet" href="<?php echo $cssBootstrap ?>" rel="stylesheet">
			
					<!-- AtomJump Feedback CSS -->
					<link rel="StyleSheet" href="<?php echo $cssFeedback ?>">
			
					<!-- Bootstrap HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
					<!--[if lt IE 9]>
					  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
					  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
					<![endif]-->
			
					<!-- Include your version of jQuery here.  This is version 1.9.1 which is tested with AtomJump Feedback. -->
					<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script> 
					<!-- Took from here 15 May 2014: http://ajax.googleapis.com/ajax/libs/jquery/1.9.1 -->
			
		<head>
		</head>
		<body class="comment-popup-body">
		 <div id="comment-popup-content" class="comment-inner-style" style="width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
			<div style="clear: both;"></div>
				<?php echo $too_many_users; ?>
		 </div>
		</body>
			
		<?php		
			
		exit(0);
	}
	
	//External video resizer link
	if($cnf['uploads']['resizeVideo']) {
			$resizer_video_url = $cnf['uploads']['resizeVideo']['url'];
			if($lang) {
				if($cnf['uploads']['resizeVideo'][$lang]) {
					$lang_converted = $cnf['uploads']['resizeVideo'][$lang];
				}
				if(!$lang_converted) {
					$lang_converted = "en";		//Default to English
				}
				$resizer_video_url = str_replace("[LANG]", $lang_converted, $resizer_video_url);
			} else {
				$resizer_video_url = str_replace("[LANG]", "en", $resizer_video_url);
			}
	} else {
		$resizer_video_url = "https://online-video-cutter.com/resize-video";		//Default
	}

	//External audio recorder link
	if($cnf['uploads']['recordAudio']) {
			$record_audio_url = $cnf['uploads']['recordAudio']['url'];
			if($lang) {
				if($cnf['uploads']['recordAudio'][$lang]) {
					$lang_converted = $cnf['uploads']['recordAudio'][$lang];
				}
				if(!$lang_converted) {
					$lang_converted = "en";		//Default to English
				}
				$record_audio_url = str_replace("[LANG]", $lang_converted, $record_audio_url);
			} else {
				$record_audio_url = str_replace("[LANG]", "en", $record_audio_url);
			}
	} else {
		$record_audio_url = "https://online-voice-recorder.com";		//Default
	}

	$upload_limits_str = str_replace("[VIDEORESIZER]", $resizer_video_url, $msg['msgs'][$lang]['uploadLimits']);	
	$upload_limits_str = str_replace("[AUDIORECORDER]", $record_audio_url, $upload_limits_str);
	
	if(isset($msg['msgs'][$lang]['mediaIssue'])) {
		$media_issue_message = $msg['msgs'][$lang]['mediaIssue'];
	} else {
		$media_issue_message = "Sorry, we were unable to capture your media. Please adjust your settings or try a different browser. Or you can upload existing media via the Upload page. Do you want to go there now?";
	}
		
	if(isset($msg['msgs'][$lang]['mediaNotSupported'])) {
		$media_not_supported = $msg['msgs'][$lang]['mediaNotSupported'];
	} else {
		$media_not_supported = "Sorry this live feature is not currently supported on this installation. You can use the Upload page, instead. Do you want to go there now?";
	}
	
	
	//Get the live video conferencing URL
	$feedback_id = $video_code . clean_output($_REQUEST['uniqueFeedbackId']);			
	
	$full_video_code = preg_replace('/[^a-zA-Z0-9]/', '', $feedback_id);    //Remove any unusual characters
						
	if($cnf['video']['url']) {
		$video_url = $cnf['video']['url'];
		
		//With a privateUrl in the config, and the forum itself being a private one, we can override the public forum URL. This way there can potentially
		//be different behaviour by e.g. an entity hosting private forums, only.
		if(($cnf['video']['privateUrl'])&&(isset($layer_info['var_public_code']))) {
			$video_url = $cnf['video']['privateUrl'];
		}
	
		//Swap a server id option with the server id being used e.g. https://jitsi32.atomjump.com where '32' is the server id
		$video_url = str_replace("[SERVERID]", $video_server, $video_url);																																			
	
		//Swap the forum text with a full forum number
		$video_url = str_replace("[FORUM]", $full_video_code, $video_url);
		


		
		if($lang) {
			if($cnf['video']['langCodeInnerVsOuter']) {
				$lang_converted = $cnf['video']['langCodeInnerVsOuter'][$lang];
			}
			if(!$lang_converted) {
				$lang_converted = "en";		//Default to English
			}
			$video_url = str_replace("[LANG]", $lang_converted, $video_url);
		}
	} else {
		$video_url = "https://jitsi0.atomjump.com/ajchangeme" . $full_video_code; 
		if($lang) {
			if($cnf['video']['langCodeInnerVsOuter']) {
				$lang_converted = $cnf['video']['langCodeInnerVsOuter'][$lang];
			}
			if(!$lang_converted) {
				$lang_converted = "en";		//Default to English
			}
			$video_url .= "?lang=" . $lang_converted;
		}
		

	}
	
	
	$android_video_app = str_replace("https://", "intent://", $video_url) . "#Intent;scheme=org.jitsi.meet;package=org.jitsi.meet;end";
	$iphone_video_app = str_replace("https://", "org.jitsi.meet://", $video_url);
	if(isset($cnf['video']['hostHttps'])) {
		$https = $cnf['video']['hostHttps'];
	} else {
		$https = false;		//a local installation, usually. We cannot use the embedded video.
	}
	
	
	if($cnf['video']['domain']) {
		
		$video_embed_domain = $cnf['video']['domain'];
		if(($cnf['video']['privateDomain'])&&(isset($layer_info['var_public_code']))) {
			$video_embed_domain = $cnf['video']['privateDomain'];
		}
		
		$video_embed_domain = str_replace("[SERVERID]", $video_server, $video_embed_domain);
		
		//Get the Jitsi embedded js file
		$video_embed_js = "https://" . $video_embed_domain . "/external_api.js";

	} else {
		//AtomJump defaults
		$video_embed_js = "https://jitsi0.atomjump.com/external_api.js";
		
		$video_embed_domain = "jitsi0.atomjump.com";
	}
	
	if($https == true) {
		$bad_permissions = "Warning: you have incorrect permissions set for your image folder, which prevents media uploads, and embedded video conferencing. Please review the installation instructions of the AtomJump Messaging Server.";
	
		$video_embed_local_js = "images/im/jitsi_external_api.js";
		
		//If there is no js file there already
		if(!file_exists(__DIR__ . "/". $video_embed_local_js)) {
			//Attempt to get the latest js file from the external server
			$latest_js_str = $ly->get_remote_ssl($video_embed_js, 1500);		//With a 1.5 second timeout
			if($latest_js_str) {
				//Have got some content
				if(!file_put_contents(__DIR__ . "/" . $video_embed_local_js, $latest_js_str)) {
					//An error writing the js file
					$warning = $bad_permissions;
				}
			} else {
			
				//Copy over the ORIGINAL version
				$original = __DIR__ . "/" . "js/jitsi_external_apiORIGINAL.js";
				$target = __DIR__ . "/". $video_embed_local_js;
				if(!copy($original, $target)) {
					//And if that fails, fallback to linking to the Jitsi
					$https = false;
					$warning = $bad_permissions;
				}
			}
		}
	}
	

	$video_conferencing_available = true;	
	if(isset($layer_info['date_video_block'])) {
		//Convert into a PHP date format
		$php_date_block = strtotime($layer_info['date_video_block']);
		if($php_date_block > time()) {
			$video_conferencing_available = false;
		}
	}
	
	
	$jitsi_options = "";
	$jitsi_json = "";
	if(isset($layer_info['var_public_code'])) {
		//This is a password protected forum - use the private options for video conferencing
		if(($cnf['video']['jitsiPrivateOptions']) && ($cnf['video']['jitsiPrivateOptions'] !== "")) {
			$jitsi_json = json_encode($cnf['video']['jitsiPrivateOptions']);
		}
	} else {
		//A public forum - use the public options
		if(($cnf['video']['jitsiPublicOptions']) && ($cnf['video']['jitsiPublicOptions'] !== "")) {
			$jitsi_json = json_encode($cnf['video']['jitsiPublicOptions']);
		}
	}
	
	//If we have jitsi options, trim off surrounding braces and append
	if($jitsi_json) {
		if ($jitsi_json[0] == '{') $jitsi_json = substr($jitsi_json,1);
		if ($jitsi_json[strlen($jitsi_json)-1] == '}') $jitsi_json = substr($jitsi_json,0,strlen($jitsi_json)-1);
		$jitsi_options = "," . $jitsi_json;		//Add a comma to make valid json, but remove surrounding braces
	}
	
	
	$display_session = "";
	if($using_session == true) { 
		$display_session = session_id(); 
	} else {
		//Otherwise, refer back to our cookie session
		if(isset($_COOKIE['ses'])) { 
		 	$display_session = $_COOKIE['ses'];
		}
	} 
	
	if(isset($cnf['uploads']['helpPrimaryLang'])) {
		$post_photo_help_link = $cnf['uploads']['helpPrimaryLang'];
		if($lang) {
			if(isset($cnf['uploads']['helpSecondaryLangs'])) {
				if($lang != $msg['defaultLanguage']) {
					//I.e. not the default language
					$post_photo_help_link = str_replace("[LANG]", $lang, $cnf['uploads']['helpSecondaryLangs']);
				}	
			}
		}	
	} else {
		$post_photo_help_link = "https://atomjump.com/wp/user-guide/#post-photo";		//Assume English
		if($lang) {
			if($lang != "en") {			
				$post_photo_help_link = "https://atomjump.com/wp/user-guide-" . $lang . "/#post-photo";
			}
		}
	}
		
	
	
	
	//Ensure no caching
	header("Cache-Control: no-store, no-cache, must-revalidate, private, no-transform"); // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header("Pragma: no-cache"); // HTTP/1.0
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	
?><!DOCTYPE html>
<html lang="en">
  <head>
  	    <meta charset="utf-8">
		 <title>AtomJump Loop - messaging for your site</title>
		 
		 <meta name="description" content="<?php echo $msg['msgs'][$lang]['description'] ?>">
		 
		 <meta name="keywords" content="<?php echo $msg['msgs'][$lang]['keywords'] ?>">
		 
			  <!-- Bootstrap core CSS -->
			<link rel="StyleSheet" href="<?php echo $cssBootstrap ?>" rel="stylesheet">
			
			<!-- AtomJump Feedback CSS -->
			<link rel="StyleSheet" href="<?php echo $cssFeedback ?>">
			
			<!-- Bootstrap HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
			
			<!-- Include your version of jQuery here.  This is version 1.9.1 which is tested with AtomJump Feedback. -->
			<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script> 
			<!-- Took from here 15 May 2014: http://ajax.googleapis.com/ajax/libs/jquery/1.9.1 -->
			
			<!-- Recording library -->
			<script src="js/recordrtc-5.6.2.js"></script>
			
			<?php if($https == true) { ?>
			<!-- Remote video conference library - but stored locally -->
			<script src="<?php echo $video_embed_local_js ?>"></script>
			<?php } ?>
			
			<script>
				var initPort = "";
				
				<?php if(isset($cnf['readPort'])) { //Support a specific port for fast reading with the loop-server-fast plugin. 'readURL' will be used instead if it is set, which gives more control.
				?>
					var readPort = "<?php echo $cnf['readPort'] ?>";
				<?php } else { ?>
					var readPort = null;
				<?php } ?>
				
				
				<?php if(isset($cnf['readURL'])) { //Support a specific URL for fast reading with the loop-server-fast plugin 
				?>
					var readURL = "<?php echo $read_url; ?>";
				<?php } else { ?>
					var readURL = null;
				<?php } ?>
				
				<?php if(isset($subdomain)) {  //Support a specific subdomain for fast reading 
				?>
					var subdomain = "<?php echo $subdomain ?>";
				<?php } else { ?>
					var subdomain = null;
				<?php } ?>
				
				
				var portReset = true;
			
				var ajFeedback = {
					"uniqueFeedbackId" : "<?php echo clean_output($_REQUEST['uniqueFeedbackId']) ?>",
					"myMachineUser" : "<?php echo clean_output($_REQUEST['myMachineUser']) ?>",
					"server" : "<?php echo trim_trailing_slash(clean_output($_REQUEST['server'])) ?>",
					"domain" : "<?php echo clean_output($_SERVER['HTTP_HOST']); ?>"
				}
				
				
				var port = "";
				
				<?php if($granted == true) { ?>
				   var granted = true;
				<?php } else { ?>
				   var granted = false;
				<?php } ?>
				
				var sendPublic = true;
				var sendPrivatelyMsg = '<?php echo $msg['msgs'][$lang]['sendPrivatelyButton'] ?>';
				var sendPubliclyMsg = '<?php echo $msg['msgs'][$lang]['sendButton'] ?>';
				var goPrivateMsg = '<?php echo $msg['msgs'][$lang]['sendSwitchToPrivate'] ?>';
				var goPublicMsg = '<?php echo $msg['msgs'][$lang]['sendSwitchToPublic'] ?>';
				var sesRAM = "<?php echo $display_session ?>";
				
				
				
			</script>
			<script type="text/javascript" src="<?php echo $root_server_url . $chat_inner_js_filename ?>"></script> 
			
		
			
			
	</head>
	<body class="comment-popup-body">
		 <div id="comment-popup-content" class="comment-inner-style" style="width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
			<div style="clear: both;"></div>
			
			
			
			<script>
				
			
				var startedFullScreen = false;
				var pfx = ["webkit", "moz", "ms", "o", ""];
				function RunPrefixMethod(obj, method) {

					var p = 0, m, t;
					while (p < pfx.length && !obj[m]) {
						m = method;
						if (pfx[p] == "") {
							m = m.substr(0,1).toLowerCase() + m.substr(1);
						}
						m = pfx[p] + m;
						t = typeof obj[m];
						if (t != "undefined") {
							pfx = [pfx[p]];
							return (t == "function" ? obj[m]() : obj[m]);
						}
						p++;
					}

				}
				
				
				function switchPublic()
				{
					//Reset any
					shortCode = "";
            		publicTo = "";
				
					if(sendPublic == true) {
						//Switch to private
						sendPublic = false;
						
						//Hide the public button
						$('#public-button').hide();
						$('#private-button').show();
						$('#private-button').html(sendPrivatelyMsg);
						
						//Show the public option on the link
						$('#private-public-link').html(goPublicMsg);
					} else {
						
						//Switch to public
						sendPublic = true;
						
						//Hide the private button
						$('#public-button').show();
						$('#private-button').hide();
						$('#public-button').html(sendPubliclyMsg);
						
						//Show the private option on the link
						$('#private-public-link').html(goPrivateMsg);
					
					}
				
					return false;
				}
				
				//From https://websparrow.org/web/javascript-password-strength-validation-example
				//Modified for multi-language, and used in notifications register.php also.
				function validatePassword(password) {
                
                var passwordStrength = {
                	"en": [ 
                				"Very weak (Tip: include special characters like %!*, numbers, Upper Case and lower case)",
                			 	"Medium",
                			 	"Strong"
                			 ],
                	"es": [ 
                				"Muy débil (Consejo: incluya caracteres especiales como %!*, números, mayúsculas y minúsculas)",
                			 	"Media",
                			 	"Fuerte"
                			 ],
                	"bg": [ 
                				"খুব দুর্বল (টিপ: %!*, সংখ্যা, বড় হাতের এবং ছোট হাতের মতো বিশেষ অক্ষর অন্তর্ভুক্ত করুন)",
                			 	"মধ্যম",
                			 	"শক্তিশালী"
                			 ],
                	"ch": [ 
                				"非常弱（提示：包括特殊字符，如 %!*、数字、大写和小写）",
                			 	"中等",
                			 	"强"
                			 ],		 
                	"cht": [ 
                				"非常弱（提示：包括特殊字符，如 %!*、數字、大寫和小寫）",
                			 	"中等的",
                			 	"強的"
                			 ],
                	"de": [ 
                				"Sehr schwach (Tipp: Sonderzeichen wie %!*, Zahlen, Groß- und Kleinbuchstaben verwenden)",
                			 	"Mittel",
                			 	"Stark"
                			 ],
                	"fr": [ 
                				"Très faible (Astuce : inclure des caractères spéciaux comme %!*, des chiffres, des majuscules et des minuscules)",
                			 	"Moyen",
                			 	"Fort"
                			 ],
                	"hi": [ 
                				"बहुत कमज़ोर (सुझाव: %!*, संख्याएँ, अपर केस और लोअर केस जैसे विशेष वर्ण शामिल करें)",
                			 	"मध्यम",
                			 	"मजबूत"
                			 ],
                	"in": [ 
                				"Sangat lemah (Tips: sertakan karakter khusus seperti %!*, angka, Huruf Besar dan huruf kecil)",
                			 	"Sedang",
                			 	"Kuat"
                			 ],
                	"it": [ 
                				"Molto debole (Suggerimento: includi caratteri speciali come %!*, numeri, maiuscole e minuscole)",
                			 	"Medio",
                			 	"Forte"
                			 ],
                	"jp": [ 
                				"非常に弱い (ヒント: %!*、数字、大文字、小文字などの特殊文字を含める)",
                			 	"中",
                			 	"強い"
                			 ],
                	"ko": [ 
                				"매우 약함(팁: %!*, 숫자, 대문자 및 소문자와 같은 특수 문자 포함)",
                			 	"보통",
                			 	"강함"
                			 ],
                	"pt": [ 
                				"Muito fraco (Sugestão: inclua caracteres especiais como %!*, números, maiúsculas e minúsculas)",
                			 	"Médio",
                			 	"Forte"
                			 ],
          			 "ru": [ 
		       				"Очень слабый (Совет: используйте специальные символы, такие как %!*, цифры, верхний и нижний регистры)",
		       			 	"Средний",
		       			 	"Сильный"
		       			 ],
          			 "pu": [ 
		       				"ਬਹੁਤ ਕਮਜ਼ੋਰ (ਸੁਝਾਅ: ਵਿਸ਼ੇਸ਼ ਅੱਖਰ ਸ਼ਾਮਲ ਕਰੋ ਜਿਵੇਂ %!*, ਨੰਬਰ, ਅੱਪਰ ਕੇਸ ਅਤੇ ਲੋਅਰ ਕੇਸ)",
		       			 	"ਦਰਮਿਆਨਾ",
		       			 	"ਮਜ਼ਬੂਤ"
		       			 ]
                	
                }
                
                // Do not show anything when the length of password is zero.
                if (password.length === 0) {
                    document.getElementById("password-msg").innerHTML = "";
                    return;
                }
                // Create an array and push all possible values that you want in password
                var matchedCase = new Array();
                matchedCase.push("[$@$!%*#?&]"); // Special Character
                matchedCase.push("[A-Z]");      // Uppercase Alphabets
                matchedCase.push("[0-9]");      // Numbers
                matchedCase.push("[a-z]");     // Lowercase Alphabets

                // Check the conditions
                var ctr = 0;
                for (var i = 0; i < matchedCase.length; i++) {
                    if (new RegExp(matchedCase[i]).test(password)) {
                        ctr++;
                    }
                }
                // Display it
                var color = "";
                var strength = "";
                switch (ctr) {
                	  case 0:
                	  		strength = "";		//Likely in  different non-latin characters - do not show anything
                	  		color = "red";
                	  break;
                    case 1:
                    case 2:
                        strength = passwordStrength[lang][0];	//"Very weak (Tip: include special characters like %!*, numbers, Upper Case and lower case)";
                        color = "red";
                        break;
                    case 3:
                        strength = passwordStrength[lang][1];   //"Medium";
                        color = "orange";
                        break;
                    case 4:
                        strength = passwordStrength[lang][2];   //"Strong";
                        color = "green";
                        break;
                }
                document.getElementById("password-msg").innerHTML = strength;
                document.getElementById("password-msg").style.color = color;
            }

				
				
			</script>
			
			
			
			
			<div id="comment-chat-form" class="container" style="padding-left: 2px; padding-right: 2px;">
				   <form id="comment-input-frm" class="form form-inline" role="form" action="" onsubmit="return mg.commitMsg(sendPublic);"  autocomplete="off" method="GET">
							<input type="hidden" name="action" value="ssshout">
							<input type="hidden" id="lat" name="lat" value="">
							<input type="hidden" id="lon" name="lon" value="">
							<input type="hidden" id="whisper_to" name="whisper_to" value="">
							<input type="hidden" id="whisper_site" name="whisper_site" value="">
							<input type="hidden" id="name-pass" name="your_name" value="<?php echo urldecode($_COOKIE['your_name']); ?>">
							<input type="hidden" name="passcode" id="passcode-hidden" value="<?php echo clean_output($_REQUEST['uniqueFeedbackId']) ?>">
							<input type="hidden" id="reading" name="reading" value="">
							<input type="hidden" name="remoteapp" value="true">
							<input type="hidden" id="clientremoteurl" name="clientremoteurl" value="<?php echo clean_output($_REQUEST['clientremoteurl']) ?>">
							<input type="hidden" id="remoteurl" name="remoteurl" value="">
							<input type="hidden" id="units" name="units" value="mi">
							<input type="hidden" id="short-code" name="short_code" value="">
							<input type="hidden" id="public-to" name="public_to" value="">
						    <input type="hidden" id="volume" name="volume" value="1.00">
							<input type="hidden" id="ses" name="ses" value="<?php echo clean_output($display_session) ?>">
							<input type="hidden" name="cs" value="21633478">
							<input type="hidden" id="typing-now" name="typing" value="off">
							<input type="hidden" id="shout-id" name="shout_id" value="">
					  		<input type="hidden" id="msg-id" name="msg_id" value="">
					   		<input type="hidden" id="message" name="message" value="">
					   		<input type="hidden" name="general" id="general-data-hidden" value="<?php echo clean_output($_REQUEST['general']) ?>">
					   		<input type="hidden" name="date-owner-start" value="<?php echo $date_start ?>">
					   		
					   		
							<input type="hidden" id="email" name="email" value="<?php if(isset($_COOKIE['email'])) { echo urldecode($_COOKIE['email']); } else { echo ''; } ?>">
							<input type="hidden" id="phone" name="phone" value="<?php if(isset($_COOKIE['phone'])) { echo urldecode($_COOKIE['phone']); } else { echo ''; } ?>">
							
							<?php if($granted == true) { ?>
								<div class="form-group col-xs-12 col-sm-12 col-md-7 col-lg-8">
								  <div class="">
										<textarea id="shouted" name="shouted" class="form-control" maxlength="420" placeholder="<?php echo $msg['msgs'][$lang]['enterComment'] ?>" autocomplete="off"></textarea>
								  </div>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-5 col-lg-4">
									<button type="submit" id="private-button"  class="btn btn-info" style="margin-bottom:3px; display: none;"><?php echo $msg['msgs'][$lang]['sendPrivatelyButton'] ?></button>
									<button type="submit" id="public-button" class="btn btn-primary" style="margin-bottom:3px;"><?php echo $msg['msgs'][$lang]['sendButton'] ?></button>
									<a href="javscript:" style="white-space: nowrap; margin-left:3px;" onclick="return switchPublic();" id="private-public-link"><?php echo $msg['msgs'][$lang]['sendSwitchToPrivate'] ?></a><?php if($https == true) { ?><a href="#" onclick="event.stopPropagation(); return showVideoConf();" style="margin-bottom:3px;"><img id="video-button" src="<?php echo $root_server_url ?>/images/video.svg" title="Video Chat" style="width: 48px; height: 32px;"></a><?php } else { //On a http site, it is usually a private server, and does not require a block - need to open in a new window ?><a target="_blank" href="<?php echo $video_url ?>" onclick="event.stopPropagation(); return true;" style="margin-bottom:3px;"><img id="video-button" src="<?php echo $root_server_url ?>/images/video.svg" title="Video Chat" style="width: 48px; height: 32px;"></a><?php } ?>
									<span id="sub-toggle"><?php echo $subscribe_toggle; ?></span><?php if(isset($cnf['recording']['supported'])&&($cnf['recording']['supported'] == true)) { ?><a href="#" onclick="event.stopPropagation(); return record();"><img id="record-button" src="<?php echo $root_server_url ?>/images/record.svg" title="Record" style="width: 32px; height: 32px;"></a><?php } //End of recordings support ?>
								</div>
							
							<?php } else { //No access so far - need to log in with the forum password ?>
								<div class="form-group col-xs-12 col-sm-12 col-md-7 col-lg-8">
								  <div class="">
									<input id="forumpass" name="forumpass" type="password" class="form-control" maxlength="510" placeholder="<?php echo $msg['msgs'][$lang]['enterForumPass'] ?>" autocomplete="new-password">
								  </div>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-5 col-lg-4">
									<button onclick="return set_options_cookie();" class="btn btn-primary" style="margin-bottom:3px;"><?php echo $msg['msgs'][$lang]['enterForumPassButton'] ?></button>
								</div>
							<?php } ?>
					</form>
			</div>
			<div id="warnings" class="alert alert-warning" role="alert" style="display: none;"></div>
			<div id="comment-prev-messages">
			</div>
			<div class="comment-forum-logged-in" style="display: none;" id="forum-logged-in">
			</div>

		</div>
		<div id="comment-options" class="comment-frm-scroller" style="width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
				<h4><?php echo $msg['msgs'][$lang]['commentSettings'] ?></h4>
				
				<div id="logged-status">
					<div style="float: right;" id="comment-logout" <?php if($_SESSION['logged-user']) { ?>style="display: block;"<?php } else { ?>style="display: none;"<?php } ?>>
					
						
						<a id="comment-logout-text" href="javascript:" onclick="beforeLogout(function() { 
							         $.get( '<?php echo $root_server_url ?>/logout.php', function( data ) { logout();  refreshLoginStatus(); } );  });" <?php if(urldecode($_COOKIE['email']) == $_SESSION['logged-email']) { ?>style="display: block;"<?php } else { ?>style="display: none;"<?php } ?>><?php echo $msg['msgs'][$lang]['logoutLink'] ?></a>
						
						<span id="comment-not-signed-in" <?php if(urldecode($_COOKIE['email']) == $_SESSION['logged-email']) { ?>style="display: none;"<?php } else { ?>style="display: block;"<?php } ?>><?php echo $msg['msgs'][$lang]['notSignedIn'] ?></span>
					</div>
				</div>
					
				 <form id="options-frm" class="form" role="form" action="" onsubmit="return set_options_cookie();"  method="POST">
				 				 <input type="hidden" name="passcode" id="passcode-options-hidden" value="<?php echo clean_output($_REQUEST['uniqueFeedbackId']) ?>">
				 				 <input type="hidden" name="general" id="general-options-hidden" value="<?php echo clean_output($_REQUEST['general']) ?>">
				 				 <input type="hidden" name="date-owner-start" value="<?php echo $date_start ?>">
				 				 <input type="hidden" id="email-modified" name="email_modified" value="false">
				 				 <div class="form-group">
				 						<div><?php echo $msg['msgs'][$lang]['yourName'] ?></div>
							 			<input id="your-name-opt" name="your-name-opt" type="text" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['enterYourName'] ?>" autocomplete="false" value="<?php if(isset($_COOKIE['your_name'])) { echo urldecode($_COOKIE['your_name']); } else { echo ''; } ?>" >
								</div>
								 <div class="form-group">
		 									<div><?php echo $msg['msgs'][$lang]['yourEmail'] ?> <a href="javascript:" onclick="$('#email-explain').slideToggle();" title="<?php echo $msg['msgs'][$lang]['yourEmailReason'] ?>"><?php echo $msg['msgs'][$lang]['optional'] ?></a><span id="subscribe-button">, <?php echo $subscribe; ?></a></span> <span id="email-explain" style="display: none;  color: #f88374;"><?php echo $msg['msgs'][$lang]['yourEmailReason'] ?></span></div>
						  					<input oninput="if(this.value.length > 0) { $('#email-modified').val('true'); $('#save-button').html('<?php if($msg['msgs'][$lang]['subscribeSettingsButton']) {
		 echo $msg['msgs'][$lang]['subscribeSettingsButton']; 
		} else { 
			echo $msg['msgs'][$lang]['saveSettingsButton'];
		} ?>'); $('#password-opt').val(''); } else { $('#email-modified').val('false'); $('#save-button').html('<?php echo $msg['msgs'][$lang]['saveSettingsButton'] ?>'); }" id="email-opt" name="email-opt" type="email" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['enterEmail'] ?>" autocomplete="username" value="<?php if(isset($_COOKIE['email'])) { echo urldecode($_COOKIE['email']); } else { 
			if(isset($_SESSION['logged-email'])) {
				echo urldecode($_SESSION['logged-email']);
			} else {
				echo '';
			}
		} ?>">
								</div>
								<div><a id="comment-show-password" href="javascript:"><?php echo $msg['msgs'][$lang]['more'] ?></a></div>
								<div id="comment-password-vis" style="display: none;">
									<div  class="form-group">
										<div><?php echo $msg['msgs'][$lang]['yourPassword'] ?> <a href="javascript:" onclick="$('#password-explain').slideToggle();" title="<?php echo $msg['msgs'][$lang]['yourPasswordReason'] ?>"><?php echo $msg['msgs'][$lang]['optional'] ?></a>, <a id='clear-password' href="javascript:" onclick="return clearPass();"><?php echo $msg['msgs'][$lang]['resetPasswordLink'] ?></a> <span id="password-explain" style="display: none; color: #f88374;"><?php echo $msg['msgs'][$lang]['yourPasswordReason'] ?> </span></div>
						  				<input oninput="if(this.value.length > 0) { $('#save-button').html('<?php if($msg['msgs'][$lang]['loginSettingsButton']) {
		 echo $msg['msgs'][$lang]['loginSettingsButton']; 
		} else { 
			echo $msg['msgs'][$lang]['saveSettingsButton'];
		} ?>'); } else { $('#save-button').html('<?php echo $msg['msgs'][$lang]['saveSettingsButton'] ?>'); }" id="password-opt" name="pd" type="password" class="form-control" autocomplete="current-password" placeholder="<?php echo $msg['msgs'][$lang]['enterPassword'] ?>" value="<?php if(isset($_REQUEST['pd'])) { echo clean_output($_REQUEST['pd']); } ?>" onkeyup="validatePassword(this.value);"><span id="password-msg"></span>
									</div>
									<div  class="form-group">
										<?php global $cnf; if($cnf['sms']['use'] != 'none') { ?>
										<div><?php echo $msg['msgs'][$lang]['yourMobile'] ?> <a href="javascript:" onclick="$('#mobile-explain').slideToggle();" title="<?php echo $msg['msgs'][$lang]['yourMobileReason'] ?>"><?php echo $msg['msgs'][$lang]['yourMobileLink'] ?></a>  <span id="mobile-explain" style="display: none;  color: #f88374;"><?php echo $msg['msgs'][$lang]['yourMobileReason'] ?></span></div>
										 <input  id="phone-opt" name="ph" type="text" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['enterMobile'] ?>" autocomplete="false" value="<?php if(isset($_COOKIE['phone'])) { echo urldecode($_COOKIE['phone']); } else { echo ''; } ?>">
										 <?php } else { ?>
										 <input  id="phone-opt" name="ph" type="hidden" placeholder="<?php echo $msg['msgs'][$lang]['enterMobile'] ?>" value="<?php if(isset($_COOKIE['phone'])) { echo urldecode($_COOKIE['phone']); } else { echo ''; } ?>">
										 <?php } ?>
									</div>
									<?php $sh->call_plugins_settings(null); //User added plugins here ?>									
									<div style="float: right;">
						  					<a id="comment-user-code" href="javascript:"><?php echo $msg['msgs'][$lang]['advancedLink'] ?></a>
						  			</div>
						  			
									<?php global $cnf;
										 $admin_user_id = explode(":", $cnf['adminMachineUser']);
						
										 if(($_SESSION['logged-user'])&&($_SESSION['logged-user'] == $admin_user_id[1])) {
										 	//Show a set forum password option, and group users form ?>
										
										 <div id="group-users-form" class="form-group" style="display:none;">
											<div><?php echo $msg['msgs'][$lang]['privateOwners'] ?> <a href="javascript:" onclick="$('#users-explain').slideToggle();" title="<?php echo $msg['msgs'][$lang]['privateOwnersReason'] ?>"><?php echo $msg['msgs'][$lang]['optional'] ?></a>  <span id="users-explain" style="display: none;  color: #f88374;"><?php echo $msg['msgs'][$lang]['privateOwnersReasonExtended'] ?></span></div>
											 <input  id="group-users" name="users" type="text" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['privateOwnersEnter'] ?>" value="">
											  <div style="padding-top:10px;"><span style="color: red;" id="group-user-count"></span> <?php echo $msg['msgs'][$lang]['subscribers'] ?></div>
										</div>
										<div id="subscribers-limit-form" class="form-group" style="display:none;">
											<div><?php echo $msg['msgs'][$lang]['limitSubscribers'] ?> <a href="javascript:" onclick="$('#subscribers-limit-explain').slideToggle();" title="<?php echo $msg['msgs'][$lang]['limitSubscribersReason'] ?>"><?php echo $msg['msgs'][$lang]['optional'] ?></a>  <span id="subscribers-limit-explain" style="display: none;  color: #f88374;"><?php echo $msg['msgs'][$lang]['limitSubscribersReasonExtended'] ?></span></div>
											 <input  id="subscribers-limit" name="subscriberlimit" type="text" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['limitSubscribersEnter'] ?>" value="<?php if(($layer_info['var_subscribers_limit']) && ($layer_info['var_subscribers_limit'] != "")) { echo $layer_info['var_subscribers_limit']; } ?>">
										</div>
										<div id="set-forum-password-form" class="form-group" style="display:none;">
											<div><?php echo $msg['msgs'][$lang]['setForumPass'] ?> <a href="javascript:" onclick="$('#forum-password-explain').slideToggle();" title="<?php echo $msg['msgs'][$lang]['setForumPassReason'] ?>"><?php echo $msg['msgs'][$lang]['optional'] ?></a>  <span id="forum-password-explain" style="display: none;  color: #f88374;"><?php echo $msg['msgs'][$lang]['setForumPassReasonExtended'] ?></span>&nbsp;<a href="javascript:" onclick="$('#set-forum-password').val('-');"><img src="images/bin.svg" width="18" height="18"></a></div>
											 <input  id="set-forum-password" name="setforumpassword" type="password" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['setForumPassEnter'] ?>" value="">
											 <input type="hidden" id="forumpasscheck" name="forumpasscheck" value="">
										</div>
										<div id="set-forum-title-form" class="form-group" style="display:none;">
											<div><?php echo $msg['msgs'][$lang]['setForumTitle'] ?> <a href="javascript:" onclick="$('#forum-title-explain').slideToggle();" title="<?php echo $msg['msgs'][$lang]['setForumTitleReason'] ?>"><?php echo $msg['msgs'][$lang]['optional'] ?></a>  <span id="forum-title-explain" style="display: none;  color: #f88374;"><?php echo $msg['msgs'][$lang]['setForumTitleReason']; ?></span></div>
											 <input  id="set-forum-title" name="setforumtitle" type="text" class="form-control" value="<?php if(($layer_info['var_title']) && ($layer_info['var_title'] != "")) { echo $layer_info['var_title']; } ?>">
										</div>
									<?php } else { ?>
							
										<div id="group-users-form" class="form-group" style="display:none;">
											<div style="padding-top:10px;"><span style="color: red;" id="group-user-count"></span> <?php echo $msg['msgs'][$lang]['subscribers'] ?></div>
											<input  id="group-users" name="users" type="hidden" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['privateOwnersEnter'] ?>" value="">
										</div>
									 	<input  id="set-forum-password" name="setforumpassword" type="hidden" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['setForumPassEnter'] ?>" value="">
									 	<input type="hidden" id="forumpasscheck" name="forumpasscheck" value="">
									 	<input type="hidden" id="subscribers-limit" name="subscriberlimit"  class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['limitSubscribersEnter'] ?>" value="">
										
									<?php } ?>
									<div id="user-id-show" class="form-group" style="display:none;">
										<div style="color: red;" id="user-id-show-set"></div>
						  			</div>
									
								</div>
								<div class="form-group">
		 									<div style="display: none; color: red;" id="comment-messages"></div>
								</div>
								<br/>
							 <button id="save-button" type="submit" class="btn btn-primary" style="margin-bottom:3px;"><?php echo $msg['msgs'][$lang]['saveSettingsButton'] ?></button>
							<br/>
							<br/>
							 <div><?php echo $msg['msgs'][$lang]['tip'] ?></div>
							 <br/>
							 <div><?php echo $msg['msgs'][$lang]['getYourOwn'] ?></div>
							 
				 </form>
		</div>
		<div id="comment-upload" class="comment-frm-scroller" style="width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
				<?php global $cnf; if($cnf['uploads']['use'] != 'none') { ?>
				<h4><?php echo $msg['msgs'][$lang]['uploadTitle'] ?></h4>
				<?php } ?>
				
				 <!-- Entries for the upload form-->
				 <input type="hidden" id="upload-layer-name" name="layer_name" value="<?php echo clean_output($_REQUEST['uniqueFeedbackId']); ?>">
				 <input type="hidden" id="upload-user-id" name="user_id" value="<?php echo $logged_user_text ?>">
				 			
				 <form id="upload-frm" class="form" role="form" onsubmit="if(anImage == true) { return upload(); } else { return uploadOther(); }" method="POST" enctype="multipart/form-data">
				 		<?php global $cnf; if($cnf['uploads']['use'] != 'none') { ?>	
				 				 <div class="form-group">
				 						<div><?php echo $msg['msgs'][$lang]['selectImagePrompt'] ?> <a id="post-photo-link" href="<?php echo $post_photo_help_link; ?>" target="_blank" href="javascript:"><img src="images/help.svg" style="width:16px; height:16px;" height="16" width="16"></a></div>
							 			<input id="image" name="fileToUpload" type="file" accept=".jpg,.jpeg,.mp3,.mp4,.pdf" class="form-control" placeholder="<?php echo $msg['msgs'][$lang]['selectImagePrompt'] ?>" multiple="multiple" data-maxwidth="<?php global $cnf; if($cnf['uploads']['hiRes']['width']) { echo $cnf['uploads']['hiRes']['width']; } else { echo '1280'; } ?>" data-maxheight="<?php global $cnf; if($cnf['uploads']['hiRes']['height']) { echo $cnf['uploads']['hiRes']['height']; } else { echo '720'; } ?>">
							 			
								</div>
								<div id="uploading-wait" style="display: none; margin-bottom: 10px;"><?php echo $msg['msgs'][$lang]['uploadingWait'] ?> <img src="images/ajax-loader.gif"></div>
								<div id="uploading-msg" style="display: none; color: #900; margin-bottom: 10px;"></div>
											<div id="preview"></div>
											 <button id="upload-button" type="submit" class="btn btn-primary" style="margin-bottom:3px; display: none;" name="submit"><?php echo $msg['msgs'][$lang]['uploadButton'] ?></button>
											 
						  	 <br/>
							 
							 <div><?php echo $upload_limits_str; ?></div>
							 <br/>
					   <?php } ?>			 
								 	<h4><?php echo $msg['msgs'][$lang]['downloadTitle'] ?></h4>
								  <div><?php echo $msg['msgs'][$lang]['downloadDescription'] ?> 	</div>
								  <br/>
						    <div><a href="download.php?format=excel&uniqueFeedbackId=<?php echo clean_output($_REQUEST['uniqueFeedbackId']) ?>" class="btn btn-primary" role="button"><?php echo $msg['msgs'][$lang]['downloadButton'] ?></a></div>
							 	 <br/>
							 <br/>
					
							 <div><?php echo $msg['msgs'][$lang]['getYourOwn'] ?></div>
							 <?php $sh->call_plugins_upload(null); //User added plugins here ?>
				 </form>
						 
				 <div id="preview-full-container">
				 	<div id="preview-full" style="height: 100%;"></div>
				 </div>
		</div>
		
		<div id="comment-emojis" class="comment-frm-scroller" style="z-index: 11000; width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
				<div style="z-index: 5000">	
				 <?php $sh->call_plugins_emojis(null); //User added plugins here ?>
				</div>
		</div>
		
		
		<div id="comment-video-conference" class="comment-frm-scroller" style="z-index: 11000; width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
				<div>
					
					<?php if($video_conferencing_available == true) { ?>
						<div id="aj-video-conf-panel" style="position: relative; float:left;"><a href="javascript:" onclick="return blockVideoConf();"><img src='<?php echo $root_server_url ?>/front-end/img/lock100.png' height="32" title="<?php echo $msg['msgs'][$lang]['videoConf']['lock']; ?>"></a> <a href="javascript:" onclick="return videoConfSwitchToChat();"><img src="<?php echo $root_server_url ?>/front-end/img/speech-bubble-start-100.png" height="32" title="Toggle to chat"></a> <a href="<?php echo $video_url ?>" target="_blank"><img src="<?php echo $root_server_url ?>/images/link.svg" height="32" title="Open in a new tab"></a> <img id="video-conference-wait" style="position: absolute; top: 120px; left: <?php echo ((clean_output($_REQUEST['width']) - 10)/2 - (26/2)) ?>px; width: 26px; z-index: 12000;" src="<?php echo $root_server_url ?>/images/ajax-loader.gif" width="26" height="26"></div>  
					<?php } else { ?>
						<div id="aj-video-conf-panel"></div>
					<?php } ?>				
					<div style="position: relative; float:right;"><a href="javascript:" onclick="return displayVideoConfClose();"><img width="16" style="margin:10px;" src="<?php echo $root_server_url ?>/images/close.svg"></a></div>
				</div>
				<div id="video-conf-display-warning" class="alert alert-warning" role="alert" style="display: none; width: 70%; margin-top: 60px;"></div>
				<div id="video-conference-show" style="z-index: 12000;"></div>
		</div>
		
		<div id="comment-record" class="comment-frm-scroller" style="z-index: 11000; width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
				<div style="z-index: 5000">	
					<form id="upload-recording-frm" class="form" role="form" onsubmit="" action="upload-photo.php" method="POST" enctype="multipart/form-data">
						 <div id="record-choice" >
						 	<div style="position: relative; float:right;"><a href="javascript:" onclick="return recordClose();"><img width="16" style="margin:10px;" src="<?php echo $root_server_url ?>/images/close.svg"></a></div>
						 	<div style="margin-left: auto; margin-right: auto; width: 140px; padding-top: 40px;">
								 	 <?php if(isset($cnf['recording']['types']['photo'])&&($cnf['recording']['types']['photo'] == true)) { ?>
									 		<a class="btn btn-danger btn-lg" role="button" style="height: 100px; padding:0px;"><div class="fakefile"><input class="file" type="file" id="mypic" accept="image/jpeg" capture="camera"></div></a><br/><br/>
									 <?php } else {  ?>
									 		<a class="btn btn-info btn-lg" role="button" style="height: 90px; width: 130px; padding:0px;" onclick="return mediaNotSupported();"><img height="90" width="130" style="" src="<?php echo $root_server_url ?>/images/camera.svg"></a><br/><br/>
									 <?php } //end of photo type ?>
									 <?php if(isset($cnf['recording']['types']['video'])&&($cnf['recording']['types']['video'] == true)) { ?>
									 		<a id="record-video-button" href="#" onclick="return recordChoice('video');" class="btn btn-danger btn-lg" role="button"><img height="64" style="" src="<?php echo $root_server_url ?>/images/video-dark.svg"></a><br/><br/>
									 <?php } else {  ?>
									 		<a id="record-video-button" href="#" class="btn btn-info btn-lg" onclick="return mediaNotSupported();" role="button"><img height="64" style="" src="<?php echo $root_server_url ?>/images/video-dark.svg"></a><br/><br/>
									 <?php } //end of video type ?>
									 <?php if(isset($cnf['recording']['types']['audio'])&&($cnf['recording']['types']['audio'] == true)) { ?>
									 	<a id="record-audio-button" href="#" onclick="return recordChoice('audio');" class="btn btn-danger btn-lg" style="width: 130px;" role="button"><img height="64" style="" src="<?php echo $root_server_url ?>/images/microphone.svg"></a><br/><br/>
									 <?php } else {  ?>
									 	<a id="record-audio-button" href="#" class="btn btn-info btn-lg"  style="width: 130px;" onclick="return mediaNotSupported();" role="button"><img height="64" style="" src="<?php echo $root_server_url ?>/images/microphone.svg"></a><br/><br/>
									 <?php } //end of audio type ?>
							 </div>
						 </div>	
						 
						 <div id="record-status" style="">
						 	<div id="record-photo-issue" style="position: relative; float:right;"><a href="javascript:" onclick="return recordClose();"><img width="16" style="margin:10px;" src="<?php echo $root_server_url ?>/images/close.svg"></a></div>
							 <div id="uploading-recording-wait" style="display: none; margin-bottom: 10px;"><?php echo $msg['msgs'][$lang]['uploadingWait'] ?> <img src="images/ajax-loader.gif"></div>
						 	 <div id="uploading-recording-msg" style="display: none; color: #900; margin-bottom: 10px;"></div>
						 </div>	 
						 <div id="record-video" style="display: none;">
							 <div style="position: relative; float:right;"><a href="javascript:" onclick="return recordClose();"><img width="16" style="margin:10px;" src="<?php echo $root_server_url ?>/images/close.svg"></a></div>
							 
							 <div id="record-video-controls" style="margin-left: auto; margin-right: auto; width: 290px; margin-top: 0px; margin-bottom: 0px; pading:0px; height: 74px; text-align: left; overflow: visible;">
								 <a id="btn-start-recording" href="#" style="width: 84px; height: 84px; margin: 0px; padding: 0px; display: inline-block;"><img width="84" height="84" style="position: relative; width: 84px; height: 84px;" src="<?php echo $root_server_url ?>/images/record.svg"></a>
								 <a id="btn-stop-recording" href="#" style="width: 84px; height: 84px; margin: 0px; padding: 0px; display: none;"><img width="84" height="84" style="width: 84px; height: 84px;" src="<?php echo $root_server_url ?>/images/stop-recording.svg"></a>
								 <span id="recording-progress" style="padding-right: 20px;"></span>
								 <a id="btn-recording-upload" href="#" class="btn btn-primary" role="button" style="display: none;"><?php echo $msg['msgs'][$lang]['uploadButton'] ?></a>				 
							</div>
							<div style="">		 
							 	<video style="padding-top:0px; margin-top: 0px; height: <?php echo clean_output($_REQUEST['height']) - 80 ?>px;" controls autoplay playsinline width="<?php echo clean_output($_REQUEST['width']) - 20 ?>" height="<?php echo clean_output($_REQUEST['height']) - 80 ?>"></video>
							</div>
						</div>
						<div id="record-audio" style="display: none">
							<button style="display: none;" id="btn-release-microphone" disabled>Release Microphone</button>
							<button style="display: none;" id="btn-download-audio-recording" disabled>Download</button>

							
						
							<div style="position: relative; float:right;"><a href="javascript:" onclick="return recordClose();"><img width="16" style="margin:10px;" src="<?php echo $root_server_url ?>/images/close.svg"></a></div>
							 
							 <div id="record-audio-controls" style="margin-left: auto; margin-right: auto; width: 290px; margin-top: 0px; margin-bottom: 0px; pading:0px; height: 74px; text-align: left; overflow: visible;">
								 <a id="btn-start-audio-recording" href="#" style="width: 84px; height: 84px; margin: 0px; padding: 0px; display: inline-block; z-index: 1000"><img width="84" height="84" style="position: relative; width: 84px; height: 84px;" src="<?php echo $root_server_url ?>/images/record.svg"></a>
								 <a id="btn-stop-audio-recording" href="#" style="width: 84px; height: 84px; margin: 0px; padding: 0px; display: none; z-index: 1000"><img width="84" height="84" style="width: 84px; height: 84px;" src="<?php echo $root_server_url ?>/images/stop-recording.svg"></a>
								 <span id="audio-recording-progress" style="padding-right: 20px;"></span>
								 <a id="btn-audio-recording-upload" href="#" class="btn btn-primary" role="button" style="display: none;"><?php echo $msg['msgs'][$lang]['uploadButton'] ?></a>				 
								 
								 
							</div>
							<div style="margin-left: auto; margin-right: auto; width: 400px;"><audio controls autoplay playsinline></audio></div>
							
						
						</div>
						<div id="record-photo" style="display: none">
       						 <canvas id="live-photo" style="display: none;"></canvas>
       					</div>
       					
				 	</form>
				</div>
		</div>
		
		<div id="comment-display-video" class="comment-frm-scroller" style="z-index: 11000; width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
			<div style="position: relative; float:right;"><a href="javascript:" onclick="return displayVideoClose();"><img width="16" style="margin:10px;" src="<?php echo $root_server_url ?>/images/close.svg"></a></div>
			<div id="comment-display-video-warning" class="alert alert-warning" role="alert" style="display: none; width: 70%"></div>		
			<video id="current-video" style="padding-top:0px; margin-top: 0px; height: <?php echo clean_output($_REQUEST['height']) - 80 ?>px;" controls autoplay playsinline width="<?php echo clean_output($_REQUEST['width']) - 20 ?>" height="<?php echo clean_output($_REQUEST['height']) - 80 ?>">
				<source src="" type="video/mp4">
				Your browser does not support inline videos.  Please try a different browser.
			</video>			
		</div>
		
		<div id="comment-display-audio" class="comment-frm-scroller" style="z-index: 11000; width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
			<div style="position: relative; float:right;"><a href="javascript:" onclick="return displayAudioClose();"><img width="16" style="margin:10px;" src="<?php echo $root_server_url ?>/images/close.svg"></a></div>
			<div id="comment-display-audio-warning" class="alert alert-warning" role="alert" style="display: none; width: 70%"></div>
			<audio id="current-audio" style="margin-top: 40px; margin-left: auto; margin-right: auto; width: 90%;" controls autoplay playsinline>
				<source src="" type="audio/mp3">
				Your browser does not support inline audio. Please try a different browser.
			</audio>			
		</div>
		
		<div id="comment-single-msg" class="comment-frm-scroller" style="z-index: 11000; width: <?php echo clean_output($_REQUEST['width']) ?>px; height: <?php echo clean_output($_REQUEST['height']) ?>px;">
				<h2>Single Message</h2>
		</div>
		
			<!-- Recording Video -->
			 <style>
			 .comment-vid-wrap {
			 	position: relative;
			 }
			 
			 .comment-vid-thumb {
			 	position: relative;
			 	top:0;
			 	left: 0;
			 	border-radius: 10px;
			 	/* img height 160 in landscape, height: 240 in portrait */
			 }
			 
			 .comment-vid-icon {
			 	position: absolute;
			 	left: 30%;
			 	top: -12px;
			 	width: 40%;
			 	height: 40px;
			 	opacity: 0.7;
			 	/* img height 40 in landscape, height: 60 in portrait */
			 }
			 
			 </style>
			 
			<script>
				<?php if($warning != "") { ?>
					alert("<?php echo $warning ?>");
				<?php } ?>
			
				var videoConfApi = null;
				var jsVidBlock = false;
				
				function displayBlockVid() {
					$('#video-conference-show').html("<img style='display: block; margin-left: auto; margin-right: auto; margin-top: 30px; width: 25%' src='<?php echo $root_server_url ?>/images/no-video.svg' width='<?php echo clean_output($_REQUEST['width']/4) ?>'>");
					var mymsg = '<?php echo $msg['msgs'][$lang]['videoConf']['locked']; ?>';
					$('#aj-video-conf-panel').html("<div style=\"position: relative; float:left;\">" + mymsg + "</div>");	
					jsVidBlock = true;
				}
				
				function showVideoConf() {
					$("#comment-video-conference").show();
					$("#comment-upload").hide();
		 	 	 	$("#comment-popup-content").hide(); 
				 	$("#comment-upload").hide();
				 	$("#comment-emojis").hide();
		 	 	 	$("#comment-record").hide();
		 	 	 	$('#video-conf-display-warning').hide();
		 	 	 	
		 	 	 	var domain = '<?php echo $video_embed_domain ?>';
					var options = {
						roomName: '<?php echo $full_video_code ?>',
						width: <?php echo (clean_output($_REQUEST['width']) - 10) ?>,
						height: <?php echo (clean_output($_REQUEST['height']) - 50) ?>,
						parentNode: document.querySelector('#video-conference-show'),
						lang: '<?php echo $lang_converted ?>',
						userInfo: {
							displayName: '<?php echo $_COOKIE['your_name'] ?>'
						},
						onload: function() {
							$('#video-conference-wait').hide();		//Stop any waiting icons
						}<?php echo $jitsi_options ?>
					};
					
					
					<?php if($video_conferencing_available == true) { ?>
						if(jsVidBlock == true) {
							displayBlockVid();
						} else {
							if(!videoConfApi) {
								$('#video-conference-wait').show();		//Stop any waiting icons
								try {
									videoConfApi = new JitsiMeetExternalAPI(domain, options);
									
									videoConfApi.executeCommand('setVideoQuality', 180);		//This was under 'TESTING' and I'm not sure if we need to remove. Seems to be working fine with it.
									
									videoConfApi.addListener("errorOccurred", function(error) {
										if(error.isFatal) {
											$('#video-conf-display-warning').html(lsmsg.msgs[lang].lostConnection);
											$('#video-conf-display-warning').show();
											$('#video-conference-wait').hide();		//Stop any waiting icons
											
										}
									});
									
								
									
								} catch(err) {
									$('#video-conf-display-warning').html(lsmsg.msgs[lang].lostConnection);
									$('#video-conf-display-warning').show();
									$('#video-conference-wait').hide();		//Stop any waiting icons
								}
							}
						}
					<?php } else { ?>
						//Set the forum window to a large blocked icon
						displayBlockVid();
					<?php } ?>
		 	 	 
		 	 	 	return false;
				}
				
				function videoConfSwitchToChat() {
					$("#comment-video-conference").hide();
					$("#comment-popup-content").show();
					return false; 
				}
				
				function displayVideoConfClose() {
					$("#comment-video-conference").hide();
					$("#comment-popup-content").show(); 
					videoConfApi.dispose();
					videoConfApi = null;
					
					return false;
				}
				
				function blockVideoConf() {
					if(confirm("<?php echo $msg['msgs'][$lang]['videoConf']['confirm'] ?>")) {
						var forumId = '<?php echo clean_output($_REQUEST['uniqueFeedbackId']) ?>';
						var data = {
							"uniqueFeedbackId": forumId
						};
						$.post(ssshoutServer + '/blk-vid.php', data, function(response) {
							if(response == "OK") {
								displayBlockVid();
							} else {
							
							}
						});
					}
				}
		 	 	 	
			
			</script>
			<script>
				function recordChoice(choice) {
					if(choice == "video") {
						$("#record-video").show();
						$("#record-choice").hide();
						$("#btn-recording-upload").hide();
					}
					
					if(choice == "audio") {
						$("#record-audio").show();
						$("#record-choice").hide();
						$("#btn-recording-upload").hide();	
						
						//On Safari, it is a two button process. 1st button asks for permission, second button starts recording.
						if(isSafari) { 
							if (!microphone) {
								captureMicrophone(function(mic) {
									microphone = mic;

								
										replaceAudio();

										audio.muted = true;
										audio.srcObject = microphone;

										
										return false;		
									
								});
								return false;		
							}
						}				
					}
					
					if(choice == "photo") {
						$("#record-photo").show();
						$("#record-choice").hide();
						$("#btn-recording-upload").hide();
						
					}
				}
				
				function recordClose() {
					$("#comment-record").hide();
					$("#comment-popup-content").show();
					
					//Release the microphone if going.
					if(microphone) {
						microphone.stop();
						microphone = null;
						
						//Stop any playing audio
						var playingAudio = $('#record-audio audio');
						playingAudio[0].pause();
					}
					
					if(video) {
						//Stop any playing video
						var playingVideo = $('#record-video video');
						playingVideo[0].pause();
					}
				}
			
				
				function mediaNotSupported(msg) {
					if(!msg) msg = "<?php echo $media_not_supported; ?>";
					if(confirm(msg)) {
						$("#comment-upload").show(); 
						$("#comment-record").hide(); 
						//Send message to the parent frame to hide highlight
						var targetOrigin = getParentUrl();		//This is in search-secure
						parent.postMessage( {'highlight': "upload" }, targetOrigin );
					
					}	
					return false;
				}
			
				var video = document.querySelector('video');
				function captureCamera(callback) {
					//video: true  gives front-facing
															
					var constraints = {
						  audio: true,
						  video: {							
							frameRate: 30,
							facingMode: "environment",
						  }
					};
					
					//width: { min: 320, ideal: 320 },
					//		height: { min: 240 },
					
					var mediaSupported = true;
					if(typeof navigator.mediaDevices === 'undefined' || !navigator.mediaDevices.getUserMedia) {
						mediaSupported = false;
						mediaNotSupported("<?php echo $media_issue_message; ?>");
						

						if(!!navigator.getUserMedia) {
							mediaSupported = false;
							mediaNotSupported("<?php echo $media_issue_message; ?>");
						}
					} 

					if(mediaSupported) {
					
						navigator.mediaDevices.getUserMedia(constraints).then(function(camera) {
							callback(camera);
						}).catch(function(error) {
							mediaNotSupported("<?php echo $media_issue_message; ?>");
							console.error(error);
						});
					}
				}

				function stopRecordingCallback() {
					video.src = video.srcObject = null;
					video.muted = false;
					video.volume = 1;
					
					video.src = URL.createObjectURL(recorder.getBlob());
					
					recordedBlob = recorder.getBlob();
					
					
					recorder.camera.stop();
					recorder.destroy();
					recorder = null;
					$("#btn-recording-upload").show();
					$("#btn-start-recording").css("display", "inline-block");	//Effectively show, but needs height
					$("#btn-stop-recording").hide();
					
					
				}

				var recorder; // globally accessible
				var recordedBlob;
				var maxSize = 9000000;

				document.getElementById('btn-start-recording').onclick = function() {
					this.disabled = true;
					captureCamera(function(camera) {
						video.muted = true;
						video.volume = 0;
						video.srcObject = camera;

						$('#recording-progress').show();


						recorder = RecordRTC(camera, {
							type: 'video',
							timeSlice: 1000 // pass this parameter
						});
						
						
						 (function looper() {
							if(!recorder) {
								return;
							}

							var internal = recorder.getInternalRecorder();
							if(internal && internal.getArrayOfBlobs) {
								var blob = new Blob(internal.getArrayOfBlobs(), {
								    type: 'video/webm'
								});


								var maxSizeUnder = maxSize * 0.8;
								var percent = Math.round((blob.size / maxSizeUnder) * 100.0);						
								$('#recording-progress').html(percent + "%"); //Produced MB size: bytesToSize(blob.size));
								if(blob.size > maxSizeUnder) {
									//Force a stopped recording
									this.disabled = true;
									recorder.stopRecording(stopRecordingCallback);
								}
							}

							setTimeout(looper, 1000);
						})();

						recorder.startRecording();

						// release camera on stopRecording
						recorder.camera = camera;

						document.getElementById('btn-stop-recording').disabled = false;
						$("#btn-stop-recording").css("display", "inline-block");		//Effectively show, but needs height
						$("#btn-start-recording").hide();
						$("#btn-recording-upload").hide();
					});
				};
				
					
					
				document.getElementById('btn-recording-upload').onclick = function() {	
					$('#uploading-recording-wait').show();
					
					// generating a random file name
                    var fileName = "hello.webm";	//getFileName('webm');

                    // we need to upload "File" --- not "Blob"
                    var fileObject = new File([recordedBlob], fileName, {
                        type: 'video/webm'
                    });

                    var formData = new FormData();

                    // recorded data
                    formData.append('video-blob', fileObject);

                    // file name
                    formData.append('video-filename', fileObject.name);
					
					formData.append("layer_name", $('#upload-layer-name').val());	
    				formData.append("user_id", $('#upload-user-id').val());	
    				formData.append("media_type", "video");			
 
					
					if(recordedBlob.size > maxSize) {
						
						alert("<?php echo $msg['msgs'][$lang]['photos']['largeError']; ?> " + recordedBlob.size + " bytes");
						$('#uploading-recording-wait').hide();		
					} else {
						$.ajax({
		                        url: ssshoutServer + '/upload-photo.php', 
		                        data: formData,
		                        cache: false,
		                        contentType: false,
		                        processData: false,
		                        dataType: "json",
		                        type: 'POST'
		               }).done(function(response) {
					
					
		                        
		                
							$('#uploading-recording-wait').hide();
						
						
							if(!response.url) {
								$('#uploading-recording-msg').html(response.msg);
								$('#uploading-recording-msg').show();
								
					
						
							} else {
								//Now wait for a second while internal images get sent around their network
								setTimeout(function(){ 
								
									//Append the response url to the input box
									//Register that we have started typing
									var link = response.url;
									if(response.msg) {
										link += "  " + response.msg;
									}
									$('#shouted').val( link );
									mg.newMsg(true);  //start typing private message
									mg.commitMsg();
					
									$('#uploading-recording-msg').html("");
									$('#uploading-recording-msg').hide();
									$("#comment-popup-content").show(); 
									$("#comment-record").hide(); 
									//Send message to the parent frame to hide highlight
									var targetOrigin = getParentUrl();		//This is in search-secure
									parent.postMessage( {'highlight': "none" }, targetOrigin );
								
								}, 1500);
							}
						});
					}
					
				};

				document.getElementById('btn-stop-recording').onclick = function() {
					this.disabled = true;
					recorder.stopRecording(stopRecordingCallback);
					
				};
			</script>
			
			
			<!-- Recording Audio -->
			<script>
				var audio = document.querySelector('audio');

				function captureMicrophone(callback) {
					btnReleaseMicrophone.disabled = false;

					if(microphone) {
						callback(microphone);
						return;
					}

					var mediaSupported = true;
					if(typeof navigator.mediaDevices === 'undefined' || !navigator.mediaDevices.getUserMedia) {
						mediaSupported = false;
						mediaNotSupported("<?php echo $media_issue_message; ?>");
						

						if(!!navigator.getUserMedia) {
							mediaSupported = false;
							mediaNotSupported("<?php echo $media_issue_message; ?>");
						}
					} 

					if(mediaSupported) {
						navigator.mediaDevices.getUserMedia({
							audio: isEdge ? true : {
								echoCancellation: false
							}
						}).then(function(mic) {
							callback(mic);
						}).catch(function(error) {
							mediaNotSupported("<?php echo $media_issue_message; ?>");
							console.error(error);
						});
					}
					
				}

				function replaceAudio(src) {
					var newAudio = document.createElement('audio');
					newAudio.controls = true;
					newAudio.autoplay = true;

					if(src) {
						newAudio.src = src;
					}
					
					var parentNode = audio.parentNode;
					parentNode.innerHTML = '';
					parentNode.appendChild(newAudio);

					audio = newAudio;
				}

				function stopRecordingAudioCallback() {
					replaceAudio(URL.createObjectURL(audioRecorder.getBlob()));

					btnStartRecording.disabled = false;

					setTimeout(function() {
						if(!audio.paused) return;

						setTimeout(function() {
							if(!audio.paused) return;
							audio.play();
						}, 1000);
						
						audio.play();
					}, 300);

					audio.play();

					btnDownloadRecording.disabled = false;

					if(isSafari) {
						click(btnReleaseMicrophone);
					}
					
					$("#btn-audio-recording-upload").show();
					$("#btn-start-audio-recording").css("display", "inline-block");	//Effectively show, but needs height
					$("#btn-stop-audio-recording").hide();

				}

				var isEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob);
				var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
				

				var audioRecorder; // globally accessible
				var microphone;

				var btnStartRecording = document.getElementById('btn-start-audio-recording');
				var btnStopRecording = document.getElementById('btn-stop-audio-recording');
				var btnReleaseMicrophone = document.querySelector('#btn-release-microphone');
				var btnDownloadRecording = document.getElementById('btn-download-audio-recording');

				btnStartRecording.onclick = function() {
					this.disabled = true;
					this.style.border = '';
					this.style.fontSize = '';
					$('#audio-recording-progress').show();

					if (!microphone) {
						captureMicrophone(function(mic) {
							microphone = mic;

							if(isSafari) {
								replaceAudio();

								audio.muted = true;
								audio.srcObject = microphone;

								btnStartRecording.disabled = false;
								btnStartRecording.style.border = '1px solid red';
								btnStartRecording.style.fontSize = '150%';
								

								alert("<?php if($msg['msgs'][$lang]['pushRecordAgain']) {
												echo $msg['msgs'][$lang]['pushRecordAgain'];
											} else {
												echo "Please close this popup, and click the highlighted 'record' button, again. (This first time accessed your microphone, while the second time will start recording.)";
											} ?>");
								return false;		
							}

							
							click(btnStartRecording);
						});
						return false;		
					}


					$("#btn-stop-audio-recording").css("display", "inline-block");		//Effectively show the 'stop' button, but needs height
					$("#btn-start-audio-recording").hide();								//Hide the start button
					$("#btn-audio-recording-upload").hide();

					replaceAudio();

					audio.muted = true;
					audio.srcObject = microphone;

					var options = {
						type: 'audio',
						numberOfAudioChannels: isEdge ? 1 : 2,
						checkForInactiveTracks: true,
						bufferSize: 16384,
						timeSlice: 1000 // For checks on size
					};

					if(isSafari || isEdge) {
						options.recorderType = StereoAudioRecorder;
					}

					if(navigator.platform && navigator.platform.toString().toLowerCase().indexOf('win') === -1) {
						options.sampleRate = 48000; // or 44100 or remove this line for default
					}

					if(isSafari) {
						options.sampleRate = 44100;
						options.bufferSize = 4096;
						options.numberOfAudioChannels = 2;
					}

					if(audioRecorder) {
						audioRecorder.destroy();
						audioRecorder = null;
					}

					audioRecorder = RecordRTC(microphone, options);
									
						 (function looper() {
							if(!audioRecorder) {
								return;
							}

							var internal = audioRecorder.getInternalRecorder();
							if(internal && internal.getArrayOfBlobs) {
								var blob = new Blob(internal.getArrayOfBlobs(), {
								    type: 'audio/mp3'
								});


								var maxSizeUnder = maxSize * 0.8;
								var percent = Math.round((blob.size / maxSizeUnder) * 100.0);						
								$('#audio-recording-progress').html(percent + "%"); //Produced MB size: bytesToSize(blob.size));
								if(blob.size > maxSizeUnder) {
									//Force a stopped recording
									this.disabled = true;
									audioRecorder.stopRecording(stopRecordingAudioCallback);
								}
							}

							setTimeout(looper, 1000);
						})();


					audioRecorder.startRecording();

					btnStopRecording.disabled = false;
					btnDownloadRecording.disabled = true;
				};

				btnStopRecording.onclick = function() {
					this.disabled = true;
					audioRecorder.stopRecording(stopRecordingAudioCallback);
					return false;
				};

				btnReleaseMicrophone.onclick = function() {
					this.disabled = true;
					btnStartRecording.disabled = false;

					if(microphone) {
						microphone.stop();
						microphone = null;
					}

					if(audioRecorder) {
						// click(btnStopRecording);
					}
					
					return false;
				};
				
				
				document.getElementById('btn-audio-recording-upload').onclick = function() {	
					$('#uploading-recording-wait').show();
					
					// generating a random file name
	                var fireFox = false;
	                var fileName = "audio.mp3";	//Although on Firefox, this will be ogx.
					if ((navigator) && (navigator.userAgent.indexOf("Firefox") > 0)) {
                		fireFox = true;
                		fileName = "audio.ogx";
            		}
            		
            		if(isSafari) {
            			//Seems to send through .wav file
            			fileName = "audio.wav";
            		}


					this.disabled = true;
					if(!audioRecorder || !audioRecorder.getBlob()) {
						alert("<?php echo $msg['msgs'][$lang]['photos']['generalError']; ?>");
						return false;
					}
					
					var blob = audioRecorder.getBlob();
					var fileObject = new File([blob], fileName, {
						type: 'audio/mp3'
					});


                    var formData = new FormData();

                    // recorded data
                    formData.append('audio-blob', fileObject);

                    // file name
                    formData.append('audio-filename', fileObject.name);
					
					formData.append("layer_name", $('#upload-layer-name').val());	
    				formData.append("user_id", $('#upload-user-id').val());			
 					formData.append("media_type", "audio");	
					
					
					if(blob.size > maxSize) {
						
						alert("<?php echo $msg['msgs'][$lang]['photos']['largeError']; ?> " + blob.size + " bytes");
						$('#uploading-recording-wait').hide();		
					} else {
						$.ajax({
		                        url: ssshoutServer + '/upload-photo.php', 
		                        data: formData,
		                        cache: false,
		                        contentType: false,
		                        processData: false,
		                        dataType: "json",
		                        type: 'POST'
		               }).done(function(response) {
					
					
		                        
		                
							$('#uploading-recording-wait').hide();
						
						
							if(!response.url) {
								$('#uploading-recording-msg').html(response.msg);
								$('#uploading-recording-msg').show();
								
					
						
							} else {
								//Now wait for a second while internal images get sent around their network
								setTimeout(function(){ 
								
									//Append the response url to the input box
									//Register that we have started typing
									var link = response.url;
									if(response.msg) {
										link += "  " + response.msg;
									}
									$('#shouted').val( link );
									mg.newMsg(true);  //start typing private message
									mg.commitMsg();
					
									$('#uploading-recording-msg').html("");
									$('#uploading-recording-msg').hide();
									$("#comment-popup-content").show(); 
									$("#comment-record").hide(); 
									//Send message to the parent frame to hide highlight
									var targetOrigin = getParentUrl();		//This is in search-secure
									parent.postMessage( {'highlight': "none" }, targetOrigin );
								
									//Release the microphone if still going.
									if(microphone) {
										microphone.stop();
										microphone = null;
									}

								
								}, 1500);
							}
						});
					}
					
				};
				
				

				btnDownloadRecording.onclick = function() {
					this.disabled = true;
					if(!audioRecorder || !audioRecorder.getBlob()) return;

					if(isSafari) {
						audioRecorder.getDataURL(function(dataURL) {
							SaveToDisk(dataURL, getFileName('mp3'));
						});
						return;
					}

					var blob = audioRecorder.getBlob();
					var file = new File([blob], getFileName('mp3'), {
						type: 'audio/mp3'
					});
					invokeSaveAsDialog(file);
					
					return false;
				};

				function click(el) {
					el.disabled = false; // make sure that element is not disabled
					var evt = document.createEvent('Event');
					evt.initEvent('click', true, true);
					el.dispatchEvent(evt);
				}

				function getRandomString() {
					if (window.crypto && window.crypto.getRandomValues && navigator.userAgent.indexOf('Safari') === -1) {
						var a = window.crypto.getRandomValues(new Uint32Array(3)),
							token = '';
						for (var i = 0, l = a.length; i < l; i++) {
							token += a[i].toString(36);
						}
						return token;
					} else {
						return (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
					}
				}

				function getFileName(fileExtension) {
					var d = new Date();
					var year = d.getFullYear();
					var month = d.getMonth();
					var date = d.getDate();
					return 'RecordRTC-' + year + month + date + '-' + getRandomString() + '.' + fileExtension;
				}

				function SaveToDisk(fileURL, fileName) {
					// for non-IE
					if (!window.ActiveXObject) {
						var save = document.createElement('a');
						save.href = fileURL;
						save.download = fileName || 'unknown';
						save.style = 'display:none;opacity:0;color:transparent;';
						(document.body || document.documentElement).appendChild(save);

						if (typeof save.click === 'function') {
							save.click();
						} else {
							save.target = '_blank';
							var event = document.createEvent('Event');
							event.initEvent('click', true, true);
							save.dispatchEvent(event);
						}

						(window.URL || window.webkitURL).revokeObjectURL(save.href);
					}

					// for IE
					else if (!!window.ActiveXObject && document.execCommand) {
						var _window = window.open(fileURL, '_blank');
						_window.document.close();
						_window.document.execCommand('SaveAs', true, fileName || fileURL)
						_window.close();
					}
				}
				</script>

				<footer style="margin-top: 20px;"><small id="send-message"></small></footer>
	
			</script>
			
		   <!-- Live Photo Capture -->
		   <style>
		    input.file {
				position: relative;
				text-align: right;
				-moz-opacity:0 ;
				filter:alpha(opacity: 0);
				opacity: 0;
				z-index: 1002;
				width: 130px; 
				height: 90px;
				cursor: pointer;
				margin-left: auto; 
				margin-right: auto;
				margin-bottom:10px;
				margin-top:6px;
			}
			

			.fakefile {
				position: relative;
				z-index: 1000;
				width: 130px; 
				height: 90px;
				background: url('images/camera.svg') no-repeat center center fixed;
				-webkit-background-size: cover;
		  		-moz-background-size: cover;
		  		-o-background-size: cover;
		  		background-size: cover;
		  		background-attachment:scroll;
	  			cursor: pointer;
	  			margin-left: auto; 
				margin-right: auto;
				margin-bottom:10px;
				margin-top:6px;
			}
			

		    </style>
		   <script>
	 
			 
			  function drawOnCanvas(file) {
			  	var reader = new FileReader();
				reader.onload = function (e) {
				  var dataURL = e.target.result,			 
				  c = document.querySelector('#live-photo'), 
				  ctx = c.getContext('2d'),
				  img = new Image();
				  
			 
				  img.onload = function() {
					c.width = img.width;		
					c.height = img.height;	
					
					//Can do for testing: 
					//ctx.drawImage(img, 0, 0, img.width, img.height);		//Show the image in it's original form
					
					var canvasb = document.createElement('canvas');
					
					  var width = img.width;
					  var height = img.height;

					

					  // calculate the width and height, constraining the proportions
					  if (width > height) {
						if (width > max_width) {
						  //height *= max_width / width;
						  height = Math.round(height *= max_width / width);
						  width = max_width;
						}
					  } else {
					  	//Swap around the max
					  	var this_max_height = max_width;
					  	var this_max_width = max_height;
						if (height > this_max_height) {
						  //width *= max_height / height;
						  width = Math.round(width *= this_max_height / height);
						  height = this_max_height;
						}
					  }
	  
					  // resize the canvas and draw the image data into it
					 canvasb.width = width;		
					 canvasb.height = height;	
					 ctxb = canvasb.getContext('2d');
					 
					  ctxb.drawImage(img, 0, 0, width, height);
					
	  
						
						
						canvasb.toBlob(function(blob) {
						  var fileObject = new File([blob], "fileName.jpg", { type: "image/jpeg" });
						  					  
						   var formData = new FormData();

				            // recorded data
				            formData.append('images', fileObject);

				            // file name
				            formData.append('name', fileObject.name);
							
							formData.append("layer_name", $('#upload-layer-name').val());	
    						formData.append("user_id", $('#upload-user-id').val());		
 							formData.append("media_type", "image");	
							
							
							$('#uploading-recording-wait').show();
							
							
							
							$.ajax({
				                    url: ssshoutServer + '/upload-photo.php', 
				                    data: formData,
				                    cache: false,
				                    contentType: false,
				                    processData: false,
				                    dataType: "json",
				                    type: 'POST'
				            }).done(function(response) {
					
								$('#uploading-recording-wait').hide();
							
							
								if(!response.url) {
									//Show the error message
									$('#uploading-recording-msg').html(response.msg);
									$('#uploading-recording-msg').show();
									$('#record-photo-issue').show();		//Allow closing of form
						
							
								} else {
									//Now wait for a second while internal images get sent around their network
									setTimeout(function(){ 
									
										//Append the response url to the input box
										//Register that we have started typing
										var link = response.url;
										
										$('#shouted').val( link );
										mg.newMsg(true);  //start typing private message
										mg.commitMsg();
						
										$('#uploading-recording-msg').html("");
										$('#uploading-recording-msg').hide();
										$("#comment-popup-content").show(); 
										$("#comment-record").hide(); 
										//Send message to the parent frame to hide highlight
										var targetOrigin = getParentUrl();		//This is in search-secure
										parent.postMessage( {'highlight': "none" }, targetOrigin );
									
									}, 1500);
								}
							});
				
						  
						  
						}, 'image/jpeg');
						
						


		               
				  };
				  		 
				  
			 	
				  //This line is not necessary - but would be used if you are drawing the image:
				  img.src = dataURL;
				  
				  
				 
				};
			 
				reader.readAsDataURL(file);
				
			  }
			 
			 

		   </script>
			
			
		
		
			<script>
			
			var fileinput = document.getElementById('image');

			var max_width = fileinput.getAttribute('data-maxwidth');
			var max_height = fileinput.getAttribute('data-maxheight');

			var preview = document.getElementById('preview');
			
			var previewFull = document.getElementById('preview-full');

			var form = document.getElementById('upload-frm');
			var anImage = false;
			var mediaType = "image";


			function uploadOther() {
			
				$('#uploading-wait').show();
			
				var formData = new FormData($('#upload-frm')[0]);
				console.log(formData);
				
				formData.append("layer_name", $('#upload-layer-name').val());	
    			formData.append("user_id", $('#upload-user-id').val());	
    			formData.append("media_type", mediaType);	
 
    						
				$.ajax({
					url: ssshoutServer + '/upload-photo.php', 
					data: formData,
					dataType: "json",
					type: 'POST',
					cache: false,
					processData: false, // Don't process the files
					contentType: false // Set content type to false as jQuery will tell the server its a query string request
				}).done(function(response) {
											
					$('#uploading-wait').hide();
					
					
					if(!response.url) {
						$('#uploading-msg').html(response.msg);
						$('#uploading-msg').show();
			
				
					} else {
						//Now wait for a second while internal images get sent around their network
						setTimeout(function(){ 
						
							//Append the response url to the input box
							//Register that we have started typing
							var link = response.url;
							if(response.msg) {
								link += "  " + response.msg;
							}
							$('#shouted').val( link );
							mg.newMsg(true);  //start typing private message
							mg.commitMsg();
			
							$('#uploading-msg').html("");
							$('#uploading-msg').hide();
							$("#comment-popup-content").show(); 
							$("#comment-upload").hide(); 
							//Send message to the parent frame to hide highlight
							var targetOrigin = getParentUrl();		//This is in search-secure
							parent.postMessage( {'highlight': "none" }, targetOrigin );
						
						}, 1500);
					}
				
				});
			
				
				return false;	
			}

			function processfile(file) {

				
  				
  				if(!( /image/i ).test( file.type ))	{
						if(( /pdf/i ).test(file.type)) {
							//A pdf file - upload directly.	
							anImage = false;
							mediaType = "pdf";
						} else {
							if(( /mpeg/i ).test(file.type)) {
								//An audio file
								anImage = false;
								mediaType = "audio";
							} else {
								if(( /mp4/i ).test(file.type)) {
									//A video file
									anImage = false;
									mediaType = "video";
								} else {
								
									//None of the above alternatives
									alert(file.name + " <?php echo $msg['msgs'][$lang]['photos']['notImage'] ?>");
									return false;
								
								}
							}
						
						}						
				} else {
					anImage = true;
				}

				
				if(anImage === true) {
					// read the files
					
					
					
					var reader = new FileReader();
										
					
					reader.readAsArrayBuffer(file);
		
					reader.onload = function (event) {
					  // blob stuff
					  var blob = new Blob([event.target.result]); // create blob...
					  window.URL = window.URL || window.webkitURL;
					  var blobURL = window.URL.createObjectURL(blob); // and get it's URL
		  
					  // helper Image object
					  var image = new Image();
					  image.src = blobURL;
			
					
					  
					  
					  //preview.appendChild(image); // preview commented out, I am using the canvas instead
					  image.onload = function() {
						// have to wait till it's loaded
												
						//For images, process and redisplay
						var resized = resizeMe(image, 0.7); // send it to canvas, 70% version
						if(!resized) {
							alert("<?php echo $msg['msgs'][$lang]['photos']['largeWarning']; ?>");
							var resized = resizeMe(image, 0.3); // send it to canvas, 30% version
							if(!resized) {
								alert("<?php echo $msg['msgs'][$lang]['photos']['selectError']; ?>");
							}
						}
						
						

						
						var newinput = document.createElement("input");
						newinput.type = 'hidden';
						newinput.name = 'images[]';		
						newinput.value = resized; // put result from canvas into new hidden input
						form.appendChild(newinput);
						
						$('#upload-button').show();		//Show the upload confirmation button
	
					  }
					}
					
					
				} else {
					//Just submit the upload directly
					
					var fileInput =  document.getElementById("image");
    				var sizeBytes = fileInput.files[0].size;
    				if(sizeBytes > 10000000) {		//10MB
    					alert("<?php echo $msg['msgs'][$lang]['photos']['largeError']; ?>");
    				} else {
						$('#upload-frm').trigger("submit");
					}
				}
			}

			function readfiles(files) {
  
				// remove the existing canvases and hidden inputs if user re-selects new pics
				var existinginputs = document.getElementsByName('images[]');
				var existingcanvases = document.getElementsByTagName('canvas');			//TODO: this conflicts
				while (existinginputs.length > 0) { // it's a live list so removing the first element each time
				  // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
				  form.removeChild(existinginputs[0]);
				  preview.removeChild(existingcanvases[0]);
				   if(previewFull.contains(existingcanvases[0])) {
				  		previewFull.removeChild(existingcanvases[0]);
				    }
				} 
				
				
  
				for (var i = 0; i < files.length; i++) {
				  processfile(files[i]); // process each file at once
				}
				fileinput.value = ""; //remove the original files from fileinput
				// TODO remove the previous hidden inputs if user selects other files
			}

			// this is where it starts. event triggered when user selects files
			fileinput.onchange = function(){
			
			  if ( !( window.File && window.FileReader && window.FileList && window.Blob ) ) {
					alert("<?php echo $msg['msgs'][$lang]['photos']['browserNoSupport']; ?>");
					return false;
				}
			  readfiles(fileinput.files);
			  
			  //Set global var in chat-inner
			  files = event.target.images;
			}

			// === RESIZE ====

			function resizeMe(img, quality) {
  				
  			   try {	
  
				  //first get a thumbnail
				  var canvas = document.createElement('canvas');
			  
			  	  var width = img.width;
				  var height = img.height;
				  var max_preview_height = 150;
				  var max_preview_width = 266;
				  if(height > width) {
				  	if (height > max_preview_height) {
					  //width *= max_height / height;
					  width = Math.round(width *= max_preview_height / height);
					  height = max_preview_height;
					}				  	 
				  } else {
				  	if (width > max_preview_width) {
					  //height *= max_width / width;
					  height = Math.round(height *= max_preview_width / width);
					  width = max_preview_width;
					}				  
				  }
				 
				  canvas.width = width;
				  canvas.height = height;
				  var ctx = canvas.getContext("2d");
				  ctx.drawImage(img, 0, 0, width, height);
				  preview.appendChild(canvas); // do the actual resized preview
			  
			  
				  //Now do the full sized version
				  var canvasb = document.createElement('canvas');

				  var width = img.width;
				  var height = img.height;

				  // calculate the width and height, constraining the proportions
				  if (width > height) {
					if (width > max_width) {
					  //height *= max_width / width;
					  height = Math.round(height *= max_width / width);
					  width = max_width;
					}
				  } else {
				  	//Swap around the max
				  	var this_max_height = max_width;
				  	var this_max_width = max_height;
					if (height > this_max_height) {
					  //width *= max_height / height;
					  width = Math.round(width *= this_max_height / height);
					  height = this_max_height;
					}
				  }
  
				  // resize the canvas and draw the image data into it
				  canvasb.width = width;
				  canvasb.height = height;
				  var ctxb = canvasb.getContext("2d");
				  ctxb.drawImage(img, 0, 0, width, height);
			  	  previewFull.appendChild(canvasb);
  
					function fullscreen(){
							var divObj = document.getElementById("preview-full");
							
							
 							var myWindow = window.open("", "", "width="+screen.availWidth+",height="+screen.availHeight);
							myWindow.document.body.appendChild(divObj);
							
							myWindow.onclick = function () {
									
									myWindow.close();
							}
							
							
							//This gets rid of the 'preview-full' element, so we need to re-create it
						
 							var newDiv = document.createElement("div"); 
 							 // and give it some content 
  							
  							newDiv.id = "preview-full";
  							
  							

 							 // add the newly created element and its content into the DOM 
  							var currentDiv = document.getElementById("preview-full-container"); 
  							currentDiv.appendChild(newDiv); 
  							
  							//And get the new element back in the global var
  							previewFull = document.getElementById('preview-full');
  							
  							//Now, limit the clickable event for the 2nd click, because that won't work
  							canvas.removeEventListener("click",fullscreen);
 							
 							
					}
 
					canvas.addEventListener("click",fullscreen);
  
  
				  return canvasb.toDataURL("image/jpeg",quality); // get the data from canvas as 70% JPG (can be also PNG, etc.)
				} catch(err) {
				  
				  	return null;		//The caller will try a slightly lower quality image.
				
				}

			}
			</script>
	
		<script>
		
			function ieVersion() {
			  var uaString = window.navigator.userAgent;
			  uaString = uaString || navigator.userAgent;
			  var match = /\b(MSIE |Trident.*?rv:|Edge\/)(\d+)/.exec(uaString);
			  if (match) return parseInt(match[2])
			}
			
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
		
		
			function clearPass()
			{
				var ur = "clear-pass.php";
				
				var email = $('#email-opt').val();
				if(email != '') {
					ur = ur + '?email=' + email;
					
					//Also save this cookie
					document.cookie = 'email=' + encodeURIComponent(email) + '; path=/; expires=' + cookieOffset() + ';';
					cookieServerSide("email", email);
				} 
				
				$('#clear-password').html("<img src=\"images/ajax-loader.gif\" width=\"16\" height=\"16\">");
			
				 $.get(ur, function(response) { 
				  		 
					   $('#clear-password').html(response);
					   
				 });
				  
				 return false;
		 	 }
		 	 
		 	 
		 	 function record() {
		 	 
		 	 	 $("#comment-upload").hide();
		 	 	 $("#comment-popup-content").hide(); 
				 $("#comment-upload").hide();
				 $("#comment-emojis").hide();
		 	 	 $("#comment-record").show();
		 	 	 $("#record-choice").show();
		 	 	 $("#record-video").hide();
		 	 	 $("#record-photo").hide();
		 	 	 $("#record-audio").hide();
		 	 	 $("#record-photo-issue").hide();
		 	 	 $("#uploading-recording-msg").html('');
				 
		 	 	 
		 	 	 return false;
		 	 }
		 
		 	  function unSub(uid, uniqueFeedbackId)
		 	  {
		 	  	//Show an ajax waiting
		 	    $('#email-explain').html("<img src=\"images/ajax-loader.gif\" width=\"16\" height=\"16\">");
		 	    $('#email-explain').show();
		 	    
		 	  	ur = "confirm.php?uid=" + uid + "&unsub=" + uniqueFeedbackId + "&validate=<?php echo $validation_code; ?>";
		 		$.get(ur, function(response) { 
				  	 if(response.includes("FAILURE") === true) {
				  	 	$('#email-explain').html("<?php echo $msg['msgs'][$lang]['problemUnsubscribing'] ?>");
					    $('#email-explain').show();
					    $('#comment-options').show();		//Show options page if we've tapped on the subscribe ear on main page
					    $("#comment-popup-content").hide(); 
					    $("#comment-upload").hide();
					    $("#comment-emojis").hide();
					    //Send message to the parent frame to show highlight
						var targetOrigin = getParentUrl();		//This is in search-secure
						parent.postMessage( {'highlight': "options" }, targetOrigin );
				  	 } else { 
				  		
					   $('#group-users').html(response);
					   $('#email-explain').html("<?php echo $msg['msgs'][$lang]['successUnsubscribing'] ?>");
					   $('#email-explain').show();
					   $('#subscribe-button').hide();
					   $('#sub-toggle').html('<?php echo $subscribe_toggle_no_ear ?>');			//Show red no listening
					 }
					   
				 });
		 	  }
		 	  
		 	   function sub(uid, uniqueFeedbackId)
		 	  {
		 	  	//Show an ajax waiting
		 	    $('#email-explain').html("<img src=\"images/ajax-loader.gif\" width=\"16\" height=\"16\">");
		 	    $('#email-explain').show();
		 	  	
		 	  	ur = "confirm.php?subscribe=true&uid=" + uid + "&sub=" + uniqueFeedbackId + "&fp=" + $("#set-forum-password").val() + "&validate=<?php echo $validation_code ?>";
		 		$.get(ur, function(response) { 
				  	 if(response.includes("FAILURE") === true) {
				  	 	$('#email-explain').html("<?php echo $msg['msgs'][$lang]['problemSubscribing'] ?>");
					    $('#email-explain').show();
					    $('#comment-options').show();		//Show options page if we've tapped on the subscribe ear on main page
					    $("#comment-popup-content").hide(); 
					    $("#comment-upload").hide();
					    $("#comment-emojis").hide();
					    //Send message to the parent frame to show highlight
						var targetOrigin = getParentUrl();		//This is in search-secure
						parent.postMessage( {'highlight': "options" }, targetOrigin );
		 	  
				  	 } else { 
				  		
					   $('#group-users').html(response);
					   $('#email-explain').html("<?php echo $msg['msgs'][$lang]['successSubscribing'] ?>");
					   $('#email-explain').show();
					   $('#subscribe-button').hide();
					   $('#sub-toggle').html('<?php echo $subscribe_toggle_ear ?>');			//Show green listening
					 } 
					   
				 });
		 	  }
		 	  
		 	  
		 	  function subFront(uid, uniqueFeedbackId) {
		 	  	
		 	  	if($("#email-opt").val() == '') {
		 	  		 $('#email-explain').show();
		 	  		 $('#comment-options').show();		//Show options page if we've tapped on the subscribe ear on main page
		 	  		 $("#comment-popup-content").hide(); 
					 $("#comment-upload").hide();
					 $("#comment-emojis").hide();
					 //Send message to the parent frame to show highlight
					 var targetOrigin = getParentUrl();		//This is in search-secure
					 parent.postMessage( {'highlight': "options" }, targetOrigin );
					 
		 	  	
		 	  	} else {
		 	  		sub(uid, uniqueFeedbackId);	
		 	  		
		 	  	}
		 	  
		 	  }
		 	  
		
				function vidDeactivate()
				{
					$('#video-button').attr("src", "<?php echo $root_server_url ?>/images/no-video.svg");
					$('#video-button').attr("title","<?php echo $msg['msgs'][$lang]['videoSupportedPlatforms'] ?>");
					$('#video-button').parent().attr("onclick", "return false;");
				}
				
				function getParentUrl() {
					var isInIframe = (parent !== window),
						parentUrl = null;

					if (isInIframe) {
						parentUrl = document.referrer;
						if(!parentUrl) parentUrl = "*";	//Handle local file case
					}

					return parentUrl;
				}
				
					
				var thisPicInput;	//Global camera input
			
				$(document).ready(function(){
					//Wait until fully loaded
					
					if(ieVersion() <= 11) {
						//In other words all versions of IE, but not Edge, which is 12+
						vidDeactivate();
					}
					
					
										
					var targetOrigin = getParentUrl();
					var msg = {'title': "<?php if(isset($layer_info['var_title'])) { echo clean_output($layer_info['var_title']); } ?>" };
					parent.postMessage(msg, targetOrigin);
					
					
					
					//Camera recordingoption			
					thisPicInput = document.querySelector('#mypic'); 
					thisPicInput.onchange = function() {
						
						var myFile = thisPicInput.files[0];
						
						$("#record-photo").show();
						$("#record-status").show();
						$("#record-choice").hide();
						$('#record-photo-issue').hide();
						drawOnCanvas(myFile);   
					};
					
					
				});
				
				
				
				$(window).on('load', function() {
					//After fully loaded page
				 	refreshLoginStatus();	//Refresh the logged in status
				});
				
				
				var myVideo;
				var myAudio;
				var videoTimeout;
				var audioTimeout;
				
				function showMedia(item, type) {
					 //Return 'true' to open the media link directly in a new tab. Or 'false' if we are to do some special handling
					switch(type) {
						case 'pdf':
							return true;
						break;
						case 'mp3':
							$("#comment-upload").hide();
					 	 	$("#comment-popup-content").hide(); 
							$("#comment-upload").hide();
							$("#comment-emojis").hide();
							$("#comment-record").hide();
							$("#comment-display-audio").show();							
							
							myAudio = $('#comment-display-audio audio');
							audioSrc = $('source', myAudio).attr('src', item.href);
							myAudio[0].addEventListener('error', function(event) { 
								//Show a waiting message and try again in 10 seconds
								$('#comment-display-audio-warning').html(lsmsg.msgs[lang].lostConnection);
								$('#comment-display-audio-warning').show();
								$('#comment-display-audio audio').hide();		//Hide the actual video tag
								audioTimeout = setTimeout(function() {
									$('#comment-display-audio audio').show();	//Reshow the actual video tag
									$('#comment-display-audio-warning').hide(); //Hide the warning again
									myAudio[0].load();
									myAudio[0].play();
								}, 10000);

							}, true);
							myAudio[0].load();
							myAudio[0].play();
							
							return false;
							
						break;
						case 'mp4':
							
							$("#comment-upload").hide();
					 	 	$("#comment-popup-content").hide(); 
							$("#comment-upload").hide();
							$("#comment-emojis").hide();
							$("#comment-record").hide();
							$("#comment-display-video").show();							
							
							myVideo = $('#comment-display-video video');
							videoSrc = $('source', myVideo).attr('src', item.href);							
							myVideo[0].addEventListener('error', function(event) { 
								//Show a waiting message and try again in 10 seconds
								$('#comment-display-video-warning').html(lsmsg.msgs[lang].lostConnection);
								$('#comment-display-video-warning').show();
								$('#comment-display-video video').hide();		//Hide the actual video tag
								videoTimeout = setTimeout(function() {
									$('#comment-display-video video').show();	//Reshow the actual video tag
									$('#comment-display-video-warning').hide(); //Hide the warning again
									myVideo[0].load();
									myVideo[0].play();
								}, 10000);

							}, true);
							myVideo[0].load();
							myVideo[0].play();
							
							return false;
						break;
						
						default:
							return true;
						break;
					}
				}
			
				function displayVideoClose() {
					$('#comment-display-video video').show();		//Make sure any video tag hidden due to an error is back on
					clearTimeout(videoTimeout);						//Prevent re-entering
					$('#comment-display-video-warning').hide();    //Hide any warning
					$("#comment-display-video").hide();
					$("#comment-popup-content").show();
					//Stop playback
					myVideo[0].pause();
				}
				
				function displayAudioClose() {
					$('#comment-display-audio audio').show();		//Make sure any audio tag hidden due to an error is back on
					clearTimeout(audioTimeout);						//Prevent re-entering
					$('#comment-display-audio-warning').hide();    //Hide any warning
					$("#comment-display-audio").hide();
					$("#comment-popup-content").show();
					//Stop playback
					myAudio[0].pause();
				}
				
				function moreClicked() {
					records=500;
					$('#moreLink').html('<img src=\"images/ajax-loader.gif\">');
					return false;
				}
			
		</script>
		
	</body>
</html>
