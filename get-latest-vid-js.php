<?php
	//Called on an hourly cron, this ensures that if the Jitsi server updates it's embedded Javascript library, our server has the latest
	//version, also.
	//sudo crontab -e
	//Add the line e.g.
	//0 * * * * 	/usr/bin/php /var/www/html/your-atomjump-api/get-latest-vid-js.php	
	
	require('config/db_connect.php');
	
	global $cnf; 

	echo "Getting latest Jitsi embedded video Javascript.  Use 'php get-latest-vid-js.php --force' to skip the random 5 minute pause.\n";
	if((isset($argv[1])) && ($argv[1] == "--force")) {
		//Don't pause
	} else {
		$pause = rand(1,300);		//Increase for more seconds randomness. 300 seconds = 5 minutes
		sleep($pause);				//This relieves the pressure on the remote server.
	}

	 
	//Set the video server to use at random
	$total = $cnf['video']['totalVideoServers'];
	$video_server = rand(1, $total) - 1;		//Starting from 0
	
	if($cnf['video']['domain']) {
		
		$video_embed_domain = $cnf['video']['domain'];
		if(($cnf['video']['privateDomain'])&&(isset($layer_info['var_public_code']))) {
			$video_embed_domain = $cnf['video']['privateDomain'];
		}
		
		$video_embed_domain = str_replace("[SERVERID]", $video_server, $video_embed_domain);
		
		//Get the Jitsi embedded js file
		$video_embed_js = "https://" . $video_embed_domain . "/external_api.js";

	} else {
		//AtomJump defaults
		$video_embed_js = "https://jitsi0.atomjump.com/external_api.js";
		
		$video_embed_domain = "jitsi0.atomjump.com";
	}
	
	$video_embed_local_js = "images/im/jitsi_external_api.js";
	
	$latest_js_str = file_get_contents($video_embed_js);
	if($latest_js_str) {
		//Have got some content
		file_put_contents(__DIR__ . "/" . $video_embed_local_js, $latest_js_str);
	} else {
		if(!file_exists(__DIR__ . "/". $video_embed_local_js)) {
			//Copy over the ORIGINAL version
			$original = __DIR__ . "/" . "js/jitsi_external_apiORIGINAL.js";
			$target = __DIR__ . "/". $video_embed_local_js;
			copy($original, $target);  
		}
	}
?>
