<?php

	//Read the total number of video servers - read by a CRON job from remote servers once a day.
	require('config/db_connect.php');
	
	global $cnf; 

	if(isset($cnf['video']['totalVideoServers'])) {
		echo $cnf['video']['totalVideoServers'];
	} else {
		echo "1";
	}
?>
