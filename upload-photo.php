<?php
	//Upload a photo to Amazon or our own server
	
	include_once(__DIR__ ."/config/db_connect.php");
	

	$target_dir = "/images/im/";

	
	
	
	function base64ToImage($base64_string, $output_file) {
		$file = fopen($output_file, "wb");

		$data = explode(',', $base64_string);

		fwrite($file, base64_decode($data[1]));
		fclose($file);

		return $output_file;
	}

	
	function clip_string($str, $length) {
		//Append dots if a larger string
		$new_str = substr($str, 0, $length);
		if(strlen($str) > ($length - 2)) {
			$new_str = substr($str, 0, $length - 2) . "..";
		}
		
		return $new_str;
	}
		

	include_once(__DIR__ ."/config/db_connect.php");

	//Include media class
	require("classes/cls.layer.php");
	
	$md = new cls_media();
	$ly = new cls_layer();
	
		
	global $cnf;
	global $msg;
	global $lang;

	//Get media uploader details 
	$media_type	= "image";	//default
	if(isset($_REQUEST['media_type'])) {
		$media_type = $_REQUEST['media_type'];
	}
	
	$layer_info = $ly->get_layer_id($_REQUEST['layer_name'], null);
	$layer_id = $layer_info['int_layer_id'];
	
	//Check if a private forum we haven't yet logged into
	if($layer_info['var_public_code']) {
		if($_SESSION['access-layer-granted']) {
				if(($_SESSION['access-layer-granted'] != $layer_info['int_layer_id'])&&(!$ly->is_layer_granted($layer_info['int_layer_id']))) {
					//Go back and get a password off the user.
					$layer_id = null;		//Error out, below
				}
		}
	}
	
	$author_id = null;
	
	if(isset($_REQUEST['user_id'])) {
		$author_id = $_REQUEST['user_id'];
	}
	if(isset($_SESSION['logged-user'])) {
		$author_id = $_SESSION['logged-user'];
	}
	
	
	
	//Check if we're a logged-in user and this matches the interface, and we have a valid layer
	if((isset($author_id))&&(isset($layer_id))) {
		$author_id = $_SESSION['logged-user'];
	} else {
		//Error case - no matching user or layer - error out and cannot upload
		error_log("Error: There was no valid user id or layer id for an uploaded media file. layer_id: " . $layer_id . "  author_id:" . $author_id);
		$message = $msg['msgs'][$lang]['photos']['generalError'];
		$url = null;
		
		$json = array("url" => $url, "msg" => $message);
		
		echo json_encode($json);
		exit(0);
	}


	//Get a random set of subfolders e.g. 3/5/2/6/3/6  to go before the filename. These will be auto-created
	$subfolders = rand(0,9) . "/" . rand(0,9) . "/" . rand(0,9) . "/" . rand(0,9) . "/" . rand(0,9) . "/" . rand(0,9) . "/";

	$_REQUEST['title'] = $subfolders . "upl" . $_SESSION['logged-user'] . "-" . rand(1,100000000);		//random image name for now - TODO better with ID
	$output_folders = __DIR__ . $target_dir . $subfolders;
	$image_path = "/images/im/";
	$message = "";
	$thumbnail = false;
	
	
	
	//Useful for future testing:
	//error_log(json_encode($_POST));			
	//error_log(json_encode($_FILES));	
		
	if(isset($_FILES['fileToUpload'])) {
		//This is a non-image file or a live image file
		//Copy the tmp file into 
		if(isset($_REQUEST['media_type'])) {
			$media_type = $_REQUEST['media_type'];
			switch($media_type) {
				case "pdf":
					$ext = ".pdf";
				break;
				
				case "video":
					$ext = ".mp4";
				break;
				
				case "audio":
					$ext = ".mp3";
				break;
				
				case "image/jpeg";
					$ext = ".jpg";
				break;
				
				default:
					$ext = ".pdf";
					$media_type	= "pdf";
				break;
			
			}
		
		} else {
			//As a fall-through, use the browser-set type which is non 100% consistent
			error_log("Warning: There was no valid media type for an uploaded media file.");
			switch($_FILES['fileToUpload']['type']) {
				case "application/pdf":
					$ext = ".pdf";
					$media_type	= "pdf";
				break;
				
				case "video/mp4":
					$ext = ".mp4";
					$media_type	= "video";
				break;
				
				case "audio/mpeg":
					$ext = ".mp3";
					$media_type	= "audio";
				break;
				
				case "image/jpeg";
					$ext = ".jpg";
					$media_type	= "image";
				break;
				
				default:
					error_log("Warning: We could not determine the media type for an uploaded media file. Listed as:" . $_FILES['fileToUpload']['type']);
					$ext = ".pdf";
					$media_type	= "pdf";
				break;
			
			}
		}
		$output_file = __DIR__ . $target_dir . $_REQUEST['title'] . $ext;
		$raw_file = $_REQUEST['title'] . $ext;
		
		//Check file is all uploaded OK
		switch ($_FILES['fileToUpload']['error']) {
		    case UPLOAD_ERR_OK:
		    	$uploaded = true;
		        break;
		    case UPLOAD_ERR_NO_FILE:
		        $message = $msg['msgs'][$lang]['photos']['generalError'];
		    	$uploaded = false;
		    case UPLOAD_ERR_INI_SIZE:
		    case UPLOAD_ERR_FORM_SIZE:
		    	$message = $msg['msgs'][$lang]['photos']['largeError'];
		    	$uploaded = false;
		    default:
		    	$message = $msg['msgs'][$lang]['photos']['generalError'];
		        $uploaded = false;
    	}

		// You should also check filesize here. 
		if ($_FILES['fileToUpload']['size'] > 10000000) {		//Max 10MB
		   $message = $msg['msgs'][$lang]['photos']['largeError'];
		   $uploaded = false;
		}
		
		
		
		//Copy the tmp file uploaded into the output file
		//Worked: copy($_FILES['fileToUpload']['tmp_name'], $output_file);
		if($uploaded == true) {
			//Create folders
			
			if(!is_dir($output_folders)) { 
				mkdir($output_folders, 0777, true); 
			}
			move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $output_file);
			
			
			
			$message = clean_output(str_replace($ext, "", $_FILES['fileToUpload']['name']));		//Returned text message alongside the link as the filename
			//Note: including a link doesn't work: $message = "<a href=\"" . $url . "\" target=\"_blank\">" . $message . "</a>";
			$message = "\"" . clip_string($message, 26) . "\"";
			

			if((($ext == ".mp4")&&($cnf['recording']['supported'] == true)&&($cnf['recording']['types']['video'] == true))||
			   (($ext == ".mp4")&&($cnf['uploads']['videoThumbnails'] == true))) {
				//This is a video which has been uploaded, and we likely have ffmpeg available. We can create a thumbnail
				
				$raw_thumb = $_REQUEST['title'] . ".jpg";
				$thumb_file = __DIR__ . $target_dir . $raw_thumb;
								
				//Now create a thumbnail .jpg of the video file.
				$response = shell_exec("ffmpeg -i " . escapeshellarg($output_file) . " -ss 00:00:00.501 -frames:v 1 " . escapeshellarg($thumb_file));		//Trying .5 seconds in so that colours have balanced
				if(!file_exists($thumb_file)) {
					//Do the very 1st frame
					$response = shell_exec("ffmpeg -i " . escapeshellarg($output_file) . " -ss 00:00:00.000 -frames:v 1 " . escapeshellarg($thumb_file));		//Trying 1st frame
				}
				
				if(file_exists($thumb_file)) {
					$thumbnail = true;
					$message = "";	//We don't want this message, it tends to look untidy alongside the filename (it wraps onto a newline underneath)
				} 
				
			}


		}
		
		
		
		
		
		
		
		
		
		if($uploaded == true) {		
			if($cnf['uploads']['use'] == "amazonAWS") {
				$url = $cnf['uploads']['vendor']['amazonAWS']['imageURL'] . $_REQUEST['title'] . $ext;
			} else {
				global $root_server_url;
				$url = $root_server_url . $image_path . $_REQUEST['title'] . $ext;		//Use hi-res version of file. 1280 pix.
			}
		} else {
			$url = null;
		}
		
		
		
		
		
		//Now copy across to the other servers (see components/upload.php)
		//also see: upload_to_all($filename, $raw_file);		
		
		//Copy across to the other servers for future reference - but do in a separate process

		global $local_server_path;
		global $cnf;
		
		if(!isset($images_script)) {
			$script = $local_server_path . "back-end/send-images-upload.phd";
		} else {
			$script = $images_script;
		
		}
		$cmd = $cnf['phpPath'] . ' ' . $script . ' ' . escapeshellarg($raw_file);
		error_log("Running " . $cmd);
		
		$response = shell_exec($cmd);
		
		
		if($thumbnail == true) {
			//Yes, we also want to share the jpeg thumbnail
			$cmd = $cnf['phpPath'] . ' ' . $script . ' ' . escapeshellarg($raw_thumb);
			error_log("Running " . $cmd);
		
			$response = shell_exec($cmd);
		}
		
		
		
		
		if($response == '') {
			//A blank response signals all clear
			$uploaded = true;
		} else {
			$uploaded = false;
			$message = $msg['msgs'][$lang]['photos']['generalError'];
		
		}
				
		
		//Register the uploaded media		
		$md->register_media_uploaded($raw_file, $url, $media_type, $layer_id, $author_id);

		
		//Export the results
		$json = array("url" => $url, "msg" => $message);
		
		echo json_encode($json);
		
	} else {
		//This is an image file or files
				
		if($_POST['images'][0]) {
			//This is a multiple file upload
			if($cnf['uploads']['use'] == "amazonAWS") {
				$output_file = __DIR__ . $target_dir . $_REQUEST['title'] . ".jpg";
				$low_res = true;			//Want two versions of the file up there.
				$resize = true;
			} else {
				$output_file = __DIR__ . $target_dir . $_REQUEST['title'] . ".jpg";
				$low_res = true;		
				$resize = true;			
			}
			
			if(!is_dir($output_folders)) { 
				mkdir($output_folders, 0777, true); 
			}
			
			base64ToImage($_POST['images'][0], $output_file);
			$uploaded = true;
			$hi_res = true;
			
			
			$images_script = __DIR__ . "/back-end/send-images-upload.phd";
			require_once(__DIR__ . "/components/upload-multiple.php");

			
		} else {
		
			$images_script = __DIR__ . "/back-end/send-images-upload.phd";
			require_once(__DIR__ . "/components/upload.php");
		}


	
	
		if($uploaded == true) {		
			switch($outgoing_type) {
		
				case "mp4":
					if($cnf['uploads']['use'] == "amazonAWS") {
						$url = $cnf['uploads']['vendor']['amazonAWS']['imageURL'] . $_REQUEST['title'] . ".mp4";
					} else {
						global $root_server_url;
						$url = $root_server_url . $image_path . $_REQUEST['title'] . ".mp4";		//Use hi-res version of file. 1280 pix.
					}
					$media_type = "video";
				break;
				
				case "mp3":
					if($cnf['uploads']['use'] == "amazonAWS") {
						$url = $cnf['uploads']['vendor']['amazonAWS']['imageURL'] . $_REQUEST['title'] . ".mp3";
					} else {
						global $root_server_url;
						$url = $root_server_url . $image_path . $_REQUEST['title'] . ".mp3";		//Use hi-res version of file. 1280 pix.
					}
					$media_type = "audio";
				break;
				
				default:
					if($cnf['uploads']['use'] == "amazonAWS") {
						$url = $cnf['uploads']['vendor']['amazonAWS']['imageURL'] . $_REQUEST['title'] . ".jpg";
					} else {
						global $root_server_url;
						$url = $root_server_url . $image_path . $_REQUEST['title'] . ".jpg";		//Use hi-res version of file. 1280 pix.
					}
					$media_type = "image";
				break;
			}
			
			//Register the uploaded media	
			$md->register_media_uploaded($raw_file, $url, $media_type, $layer_id, $author_id);
			
		} else {
			$url = null;
		}
		
		

		
		$json = array("url" => $url, "msg" => $message);
				
		echo json_encode($json);
	}

?>

