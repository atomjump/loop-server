<?php

	//Set cookies - particularly needed for iPhones which don't support being set in Javascript directly using document.cookie particularly well.
	//See https://www.cookiestatus.com/safari/  and https://www.cookiestatus.com/
	
	function clean_data($string) {
	  //Use for cleaning input data before addition to database
	  if (get_magic_quotes_gpc()) {
	    $string = stripslashes($string);
	  }
	  $string = strip_tags($string);
	  return $string;
	}
	
	function clean_output($string) {
	  //Use for cleaning data before display
	  $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
	  return $string;
	}

	//Sanitise
	$name = clean_data($_REQUEST['name']);
	$value = clean_data($_REQUEST['value']);
	$params = clean_data($_REQUEST['params']);

	//Old-style (prior to PHP7.3) header set directly:
	$days_forward = "+" . ((365*3)+2) . " day";		//The +2 is so we can compare the cookie set via the server with the client side cookies
	$expires = date("D, d M Y H:i:s",strtotime($days_forward)) . ' GMT';
	$header = "Set-Cookie: " . $name . "=" . $value . "; expires=" . $expires . "; Path=/; SameSite=Strict;";
	header($header);		//E.g. 'Set-Cookie: mycookie=value; SameSite=Strict;'
	
	
	//New style, alternative >= PHP7.3
	/*$arr_cookie_options = array (
                'expires' => time() + 60*60*24*((365*3)+2),		//Three years from now + 1 day. For checking this is actually the one used.
                'path' => '/', 
                'SameSite' => 'Strict' // None || Lax  || Strict
                );
              
    setcookie($name, $value, $arr_cookie_options); 
    */
    
    //Ensure no caching
	header("Cache-Control: no-store, no-cache, must-revalidate, private, no-transform"); // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header("Pragma: no-cache"); // HTTP/1.0
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    
    echo clean_output($name) . " " . clean_output($value) . " " . clean_output($params);
?>
