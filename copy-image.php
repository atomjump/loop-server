<?php
	//copy image
	
	if(isset($_POST['targetpath'])) {
		$target_path = $_POST['targetpath'];
	} else {
		$target_path = "/images/property/";
	}
	
	$encoded_file=$_POST['file'];
	$decoded_file=base64_decode($encoded_file);
	
	//Filter allowed target paths:
	$allowed_paths = [ "/images/im/", "/images/property/" ];
	$file_allowed = false;
	for($cnt = 0; $cnt < count($allowed_paths); $cnt++) {
		if($target_path === $allowed_paths[$cnt]) {
			$file_allowed = true;		
		}
	}
	
	$dest = getcwd() . $target_path . $_POST['FILENAME'];
	
	
	if($file_allowed == true) {	
		//Create the correct subfolders if they don't already exist
		$output_folders = dirname($dest);
		if(!is_dir($output_folders)) { 
			mkdir($output_folders, 0777, true); 
		}
	
		/*Now you can copy the uploaded file to your server.*/
		file_put_contents($dest,$decoded_file);
		
	} else {
		echo "Sorry, only particular paths are allowed.";
	}

?>
