<?php 
 require('config/db_connect.php');
 
 
	//For plugins - language change in particular
	require($start_path . "classes/cls.layer.php");
	require($start_path . "classes/cls.ssshout.php");
	
	require($start_path . "classes/cls.pluginapi.php");
	
	$ly = new cls_layer();
	$sh = new cls_ssshout();
	$api = new cls_plugin_api(); 
 
 
 	
	
 
 global $cnf; 
 global $msg;
 global $lang;
 $screen_type = "resetpass";	//TODO: change to "resetpass"
 
 $subdomain = check_subdomain();

 $webroot = web_root($cnf);	
 
 
 include("components/inner_js_filename.php");
 
 
if(isset($cnf['email']['sending']['vendor']['mailgun']['key'])) {
	$unique_pass_reset = $cnf['db']['user'] . $cnf['email']['sending']['vendor']['mailgun']['key'];	//This should be unique per installation.	
} else {
	$unique_pass_reset = $cnf['db']['user'] . $cnf['db']['pass'];	//Should also be unique per installation - the number is not shown.
}





    
 if($_REQUEST['action']) {
   
    //Get default messages to display
   	$main_message = "";
	$follow_on_link = "https://atomjump.com";
	if($cnf['serviceHome']) {
		$follow_on_link = add_subdomain_to_path($cnf['serviceHome']);
	}
	
	$first_button_wording = "&#8962;";		//A 'home' UTF-8 char
	if(isset($msg['msgs'][$lang]['backHome'])) {		//"Go to the forum"
		$first_button_wording = $msg['msgs'][$lang]['backHome'];
	} 

	$first_button = $follow_on_link . "?autostart=true";		//Open the forum automatically if atomjump.com
   
   $email = $_REQUEST['user'];
   
   //decrypt 
   $action = md5(date('Ymd') . $email . $unique_pass_reset);
   if($action == $_REQUEST['action']) {
       //check is valid timewise
   
       //clear user's password (and confirm their account at the same time, if not previously confirmed)
       
       $sql = "UPDATE tbl_user SET var_pass = NULL, enm_confirmed = 'confirmed' WHERE var_email = ?";
       $result = dbquery($sql, [["s", strtolower($email)]])  or die("Unable to execute query $sql " . dberror());
			
			
		 $main_message = $msg['msgs'][$lang]['pass']['title'];        
		       	
          
       //Password cleared
          
    } else {
    	  $main_message = $msg['msgs'][$lang]['passwordNotReset'];
    
    }
    
    
    $display_email = $email;
	 include("components/basic-input-page.php");
    
 
 } else {
	
	  
		$email = $_REQUEST['email'];
		if($email == '') {
			 $email = $_SESSION['logged-email'];
		} else {
			   $sql = "SELECT * FROM tbl_user WHERE var_email = ?";
      		$result = dbquery($sql, [["s", strtolower($email)]])  or die("Unable to execute query $sql " . dberror());
			   if($row = db_fetch_array($result)) {
			      //There is an email like this on the system   
			   		 
			   } else {
			   		echo $msg['msgs'][$lang]['emailNotExist'] . " ";		//But allow through so that it tries to send an email still.
			   		$email = "";														//But blank out the email, so that the message isn't confusing.
			   																				//It will likely show 'Please enter your email above. Then click here'
			   }
			
		
		}
	
	  if($email != '') {
	     //Send an email to the logged email
	     $link = $root_server_url . '/clear-pass.php?action=' . md5(date('Ymd') . $email . $unique_pass_reset)  . '&user=' . $email; 
	     cc_mail_direct($email, strip_tags($msg['msgs'][$lang]['pass']['title']), $msg['msgs'][$lang]['pass']['pleaseClick'] ."<a href=\"$link\">$link</a>", $cnf['email']['webmasterEmail']);
	     echo $email . ": " . $msg['msgs'][$lang]['pass']['checkAndClick'];
	  } else {
	  	 echo $msg['msgs'][$lang]['pass']['pleaseEnterEmail'];
	  }
	
 }
 
 
?>
