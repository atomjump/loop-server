<?php

require('config/db_connect.php');

require("classes/cls.basic_geosearch.php");
require("classes/cls.layer.php");
require("classes/cls.ssshout.php");

//Confirm the user and password are correct - display after clicking advanced


$lg = new cls_login();
$ly = new cls_layer();


global $cnf; 
global $msg;
global $lang;



if($_REQUEST['usercode']) {

	//Get the layer id
	$ly->get_layer_id($_REQUEST['passcode'], null);

	//Read
	$json = $lg->get_usercode();
	
} else {
	if(isset($_REQUEST['unsub'])) {
		//Unsubscribe - just needs an email address and a layer id
		if(isset($_REQUEST['human'])) {	
			$validated = $lg->validate($_REQUEST['uid'], $_REQUEST['human']);
		} else {
			$validated = $lg->validate($_REQUEST['uid'], $_REQUEST['validate']);
		}
		
		if($validated == true) {
			$json = $lg->unsubscribe($_REQUEST['uid'], $_REQUEST['unsub']);
		} else {
			$json = "FAILURE";
		}
		
		if(isset($_REQUEST['human'])) {	
			if($json == "FAILURE") {
				$main_message = $msg['msgs'][$lang]['problemUnsubscribing'];
			} else {
				$main_message = $msg['msgs'][$lang]['successUnsubscribing'];
			}
			
			$first_button_wording = "⌂";		//Home
			
			$follow_on_link = "https://atomjump.com";
			if($cnf['serviceHome']) {
				$follow_on_link = add_subdomain_to_path($cnf['serviceHome']);
			}
			$first_button = $follow_on_link;
			include_once("components/basic-page.php");
			exit(0);
		}
	} else {
		if((isset($_REQUEST['subscribe']))&&($_REQUEST['subscribe'] == "true")) {			//Clicking on the ear. Only called by sub() func in search-secure.php
			//Subscribe
			
		
			$validated = $lg->validate($_REQUEST['uid'], $_REQUEST['validate']);
				
			if($validated == true) {
				$json = $lg->subscribe($_REQUEST['uid'], $_REQUEST['sub'], $_REQUEST['fp']);
			} else {
				$json = "FAILURE";
			}			
			
		
		} else {
			//Confirm email/password, and/or create a new user
		
			$json = $lg->confirm($_REQUEST['email-opt'], $_REQUEST['pd'], $_REQUEST['ph'], $_REQUEST['users'], $_REQUEST['passcode'], false, $_REQUEST);
			
			//Confirm this layer
			$ly->get_layer_id($_REQUEST['passcode'], null);			
			
		}
	}
}

echo $_GET['callback'] . "(" . json_encode($json) . ")";


?>
