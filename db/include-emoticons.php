<?php
	//Install emoticon support in an existing database
	//Warning: this script will not work if you have more than one AtomJump database
	chdir(__DIR__ . "/../");
	require(__DIR__ . '/../config/db_connect.php');
	
	
	$sql = "DROP INDEX `shouted_already` ON tbl_ssshout";
	dbquery($sql) or die("Unable to execute query $sql " . dberror());	
	
	$sql = "ALTER TABLE tbl_ssshout ADD INDEX `shouted_already` (`var_ip`,`var_shouted`(191),`date_when_shouted`)";
	dbquery($sql) or die("Unable to execute query $sql " . dberror());
	
	$sql = "ALTER TABLE tbl_ssshout CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin";
	dbquery($sql) or die("Unable to execute query $sql " . dberror());	
	
	$default_db_name = "atomjump";
	global $db_name;
	if(isset($db_name)) $default_db_name = $db_name;
	$sql = "ALTER SCHEMA " . $default_db_name . " DEFAULT CHARACTER SET utf8mb4  DEFAULT COLLATE utf8mb4_bin";
	dbquery($sql) or die("Unable to execute query $sql " . dberror());	
	
	echo "Database Updated Successfully. Warning: if you have more than one database, you should run the SQL commands manually on each.\n";
?>
