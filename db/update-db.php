<?php

	chdir(__DIR__ . "/../");
	require(__DIR__ . '/../config/db_connect.php');




	//Each entry in the array has a git version number of the main loop-server project
	$updates = ["v4.2.0" => [ "check" => "SELECT int_video_server FROM tbl_layer",
							  "onFailure" => ["ALTER TABLE tbl_layer ADD `int_video_server` INT(10) DEFAULT NULL",
								 						 "ALTER TABLE tbl_layer ADD `date_video_block` datetime DEFAULT NULL"]
								 ],
				"4.5.7" =>  [ "check" => "SELECT * FROM tbl_media",
							  "onFailure" => ["CREATE TABLE `tbl_media` (
								  `int_media_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
								  `int_author_id` int(11) NOT NULL,
								  `int_layer_id` int(11) NOT NULL,
								  `int_ssshout_id` int(11) DEFAULT NULL,
								  `var_filename` varchar(255) DEFAULT NULL,
								  `var_url` varchar(255) DEFAULT NULL,
								  `enm_uploaded` enum('true','false') COLLATE utf8_bin DEFAULT 'true',
								  `enm_active` enum('true','false') COLLATE utf8_bin DEFAULT 'true',
								  `enm_has_hi_res` enum('yes','no') DEFAULT 'no',
								  `enm_type` enum('image','video','pdf','audio') DEFAULT 'image',
								  `enm_source` enum('camera','ai','cgi','artist','unknown') DEFAULT 'unknown',
								  `enm_source_known` enum('yes','no','unsure') DEFAULT 'unsure',
								  `flt_source_certainty` decimal(5,5) DEFAULT '0.0000',
								  `date_created` datetime DEFAULT NULL,
								  `date_updated` datetime DEFAULT NULL,
								  `date_deleted` datetime DEFAULT NULL,
								  PRIMARY KEY (`int_media_id`),
								  KEY `tbl_media_from_layer` (`int_layer_id`, `enm_active`),
								  KEY `tbl_media_from_message` (`int_ssshout_id`, `enm_active`),
								  KEY `tbl_media_from_file` (`var_filename`, `enm_active`),
								  KEY `tbl_media_from_url` (`var_url`, `enm_active`),
								  KEY `tbl_media_from_user` (`int_author_id`, `enm_active`)
								) ENGINE=InnoDB AUTO_INCREMENT=414 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;"]
						]
				];
				
	$errors = 0;
	$cnt = 0;
	
	foreach($updates as $key => $version) {
		$check_sql = $version['check'];
		$result = dbquery($check_sql);
		if(!$result) {
			//Failed check. Run each new SQL entry  
			echo "Failed check for version " . $key . "  SQL:" . $check_sql . ". Installing new updates\n";
			foreach($version['onFailure'] as $sql) {
				echo "\n\nAbout to run: " . $sql . "\n";
				$result = dbquery($sql);  
				if(!$result) {
					echo "Unable to execute query $sql " . dberror();
					$errors ++;
				} else {
					echo "Success\n";
				}
			}	
		} else {
			echo "Passed check for version " . $key . "  SQL:" . $check_sql . "\n";
		}
		$cnt++;
	}		
	
	if($errors > 0) {
		echo "\n\nSorry there were " . $errors . " errors. Please check the returned errors, above.\n";
	}	

?>
