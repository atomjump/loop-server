<?php
	//Cron job to remove and trailing 'typing' entries after 15 minutes. We can run this every 5 minutes, but make sure it is only on one machine.
	//To install put the following line in after typing 
	//		sudo crontab -e
	//		*/5 * * * *		/usr/bin/php /var/www/html/loop-server/typing-cron.php
	
	//Not sure why this was in here at one stage? An identity thing, perhaps, to prevent spammers?
	//$agent = "AJ feed bot - https://atomjump.com";
	//ini_set("user_agent",$agent);
	//$_SERVER['HTTP_USER_AGENT'] = $agent;


	//Allow us to run this script with a layer name, so that e.g. api0 or api1 or api2 can be called, which would correspond with different databases in the config:
	//		*/5 * * * *		/usr/bin/php /var/www/html/loop-server/typing-cron.php api1_update
	if(isset($argv[1])) {		
		$_REQUEST['passcode'] = $argv[1];
	}


	include_once(__DIR__ . '/config/db_connect.php');		



	$sql = "UPDATE tbl_ssshout SET enm_active = 'false' where enm_active = true and enm_status = 'typing' and date_when_shouted < DATE_SUB(NOW(),INTERVAL 15 MINUTE)";
	dbquery($sql) or die("Unable to execute query $sql " . dberror());
	
?>
