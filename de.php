<?php

//Deactivate a message
require('config/db_connect.php');

require("classes/cls.basic_geosearch.php");
require("classes/cls.layer.php");
require("classes/cls.ssshout.php");


$bg = new clsBasicGeosearch();
$ly = new cls_layer();
$sh = new cls_ssshout();


global $cnf;
global $msg;
global $lang;

if(isset($_REQUEST['just_typing'])) {
	$just_typing = true;
} else {
	$just_typing = false;
}

//Must include a layer id in $_REQUEST['passcode'] also, because it could be from a completely different database in the case of scaleUp.

if($_REQUEST['passcode']) {

	$resp_str = $sh->deactivate_shout($_REQUEST['mid'], $just_typing);
} else {
	
	
	if($msg['msgs'][$lang]['failureDeactivating']) {
		$resp_str = $msg['msgs'][$lang]['failureDeactivating'];
	} else {
		$resp_str = "Sorry we could not deactivate the AtomJump Message. Please contact the system admin.";
	}

	
}

//For now, let anyone remove messages
$ip = $ly->getRealIpAddr();

$json = "";

//This is a jquery ajax json call, so we need a proper return
if(isset($_GET['callback'])) {
	echo $_GET['callback'] . "(" . json_encode($json) . ")";
} else {
	
	
	//Just the word "reset"	
	$main_message = $resp_str;
	
	
	$first_button_wording = "⌂";		//Home
	
	$follow_on_link = "https://atomjump.com";
	if($cnf['serviceHome']) {
		$follow_on_link = add_subdomain_to_path($cnf['serviceHome']);
	}
	$first_button = $follow_on_link;	
	
	include('components/basic-page.php');
}

?>
